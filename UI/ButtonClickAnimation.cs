﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonClickAnimation : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private Vector2 originScale;
    private float shrinkScale = 0.8f;
    void Awake()
    {
        originScale = transform.localScale;
    }

    void OnEnable()
    {
        transform.localScale = originScale;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        transform.localScale = originScale * shrinkScale;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = originScale;
    }
}
