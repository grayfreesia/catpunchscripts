﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageDropdown : MonoBehaviour
{
    private Text label;
    private Dropdown dropdown;
    private List<string> langs;
    void Awake()
    {
        dropdown = GetComponent<Dropdown>();
        label = GetComponentInChildren<Text>();
    }

    void Start()
    {
        langs = new List<string>(LanguageManager.Instance.GetLangNames());
        dropdown.ClearOptions();
        dropdown.AddOptions(langs);
        dropdown.value = LanguageManager.Instance.num;
    }

    void OnEnable()
    {
        dropdown.value = LanguageManager.Instance.num;
        LanguageManager.Instance.SetFont(label);
    }

    public void OnValueChanged()
    {
        LanguageManager.Instance.OnChangeLanguage(dropdown.value);
        LanguageManager.Instance.SetFont(label);
    }



}
