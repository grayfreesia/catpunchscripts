﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailUIAnimation : MonoBehaviour
{
    public float delay = 0f;
    public Vector2 from = new Vector2(0f, -1000f);
    private RectTransform rectTransform;
    private Vector3 originPos;
    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        originPos = rectTransform.anchoredPosition;
    }


    void OnEnable()
    {       
        Invoke("Move", delay);
        rectTransform.anchoredPosition = from;
    }

    void OnDisable()
    {
        iTween.Stop(gameObject);
    }

    public void Move()
    {
        // if(rectTransform == null) return;
        Vector2 curPosion = from;
        Vector2 desPosion = originPos;

        // from 과 to 사이의 보간 값을 OnUIMove()의 매개변수로 전달한다.
        iTween.ValueTo(gameObject, iTween.Hash("time", .5f, "from", curPosion, "to", desPosion, "onupdate", "OnUIMove", "easetype", iTween.EaseType.easeOutBack));
    }
 
    private void OnUIMove(Vector2 targetPosion)
    {
        // UI 의 좌표 이동은 anchoredPosion 으로 한다.
        rectTransform.anchoredPosition = targetPosion;
    }
}
