﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteManager : MonoBehaviour
{
    private const int totalType = 5;
    public int blockSkin = 0;
    public Sprite[] sprites;
    // public Sprite[] basicBlock;
    // public Sprite[] goldBlock;
    // public Sprite[] goldBonusBlock;
    // public Sprite[] autoPunchBlock;
    // public Sprite[] iceBlock;
    
    public static SpriteManager Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Start()
    {
        SetBlockSkin(ShopDataManager.Instance.curBlock);
    }

    public Sprite GetSprite(int num, int type)
    {
        if(sprites.Length <= num * totalType + type) return null;
        return sprites[num * totalType + type];
    }

    public void SetBlockSkin(int num)
    {
        blockSkin = num;
        foreach(Block block in Map.Instance.blocks)
        {
            block.SetBlockShape();
        }
    }

}