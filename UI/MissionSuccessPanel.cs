﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionSuccessPanel : MonoBehaviour
{
    public Text title;
    public Image item;
    public Image bgItem;
    public GameObject bg;
    public Text nameText;
    public Sprite goldSprite;
    public Text topText;
    public Text getText;
    private string topString = "{0}";

    private int type = -1;
    private int num = -1;
    private bool isPaused;

    void OnEnable()
    {
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground(false);
        SetString();
    }
   
    private void SetString()
    {
        LanguageManager.Instance.SetFont(title, topText, getText);
        title.text = LanguageManager.GetString(0, 21);
        topString = LanguageManager.GetString(0, 22);
        getText.text = LanguageManager.GetString(0, 3);
    }

    public void OnClickConfirm()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void SetData(int type, int num)
    {
        Debug.Log("set data");
        this.type = type;
        this.num = num;
        topText.text = string.Format(topString, Mission.Instance.missionObjectNum);

        if(type == -1)
        {
            item.gameObject.SetActive(true);
            bg.SetActive(false);
            item.sprite = goldSprite;
            nameText.text = "X" + num.ToString();
        }
        else if(type == 2)
        {
            item.gameObject.SetActive(false);
            bg.SetActive(true);
            bgItem.sprite = ShopDataManager.Instance.GetSprite(type, num);
            nameText.text = ShopDataManager.Instance.GetName(type, num);
        }
        else
        {
            item.gameObject.SetActive(true);
            bg.SetActive(false);
            item.sprite = ShopDataManager.Instance.GetSprite(type, num);
            nameText.text = ShopDataManager.Instance.GetName(type, num);
            if(type == 0)
            {
                item.transform.localScale = Vector2.one;
            }
            else
            {
                item.transform.localScale = Vector2.one * .5f;
            }
        }

        GetItem();
    }

    public void OnBack()
    {
        SetItem();
        PopupManager.Instance.PopupActive(gameObject, false);
    }


    void GetItem()
    {
        Debug.Log("get item");
        if(type == -1)
        {
            Gold.Instance.AddGold(num);
            // Gold.Instance.GiftEffect();
        }
        else
        {
            ShopDataManager.Instance.SetPossessionTrue(type, num);
            if (type == 0)
            {
                PlayerPrefs.SetInt("CurCat", num);
            }
            else if (type == 1)
            {
                PlayerPrefs.SetInt("CurBlock", num);

            }
            else
            {
                PlayerPrefs.SetInt("CurBG", num);
            }
        }
    }  

    void SetItem()
    {
        if(type == -1)
        {
            Gold.Instance.GoldNumTween(Gold.Instance.GetGold() - num, Gold.Instance.GetGold(), 1.1f);
            Gold.Instance.GiftEffect();
        }
        else
        {
            if (type == 0)
            {
                ShopDataManager.Instance.curCat = num;
                PlayerAnimation.Instance.SetCatNum(num);
            }
            else if (type == 1)
            {
                ShopDataManager.Instance.curBlock = num;
                SpriteManager.Instance.SetBlockSkin(num);
            }
            else
            {
                ShopDataManager.Instance.curBG = num;
                Background.Instance.SetSprite();
            }
        }
    }

}
