﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseManager : MonoBehaviour
{
    private int[] goldAmount = { 200, 3000, 18000, 45000 };
    private string[] productIDs = {"", "catpunchgold_3000", "catpunchgold_18000", "catpunchgold_45000",
                                    "catpunchpack1", "catpunchpack2", "catpunchpack3"};
    public IAPManager iap;
    public GameObject packagePanel;
    public GameObject purchaseSueccess;

    public bool[] isPackPurchased = { false, false, false };
    public const int packStartNum = 4;
    public bool isDebug = false;
    private Action onSuccess;
    public static PurchaseManager Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        GetPackPurchased();
    }

    public void OnClickPurchase(int num, Action callback)
    {
        onSuccess = callback;
        if (isDebug)
        {
            OnPurchase(num);
        }
        else
        {
            iap.BuyProductID(productIDs[num]);
        }
    }

    public void OnPurchase(int num)
    {
        if (num < 4)
        {
            Gold.Instance.AddGold(goldAmount[num], 0f);
        }
        else if (num == 4)//어흥냥이팩
        {
            int gold = 8000;
            gold += PurchaseItem(0, 8);
            gold += PurchaseItem(1, 10);
            gold += PurchaseItem(2, 3);
            Gold.Instance.AddGold(gold, 0f);
            Debug.Log(gold);
        }
        else if (num == 5)//하트냥이팩
        {
            int gold = 8000;
            gold += PurchaseItem(0, 7);
            gold += PurchaseItem(1, 11);
            gold += PurchaseItem(2, 4);
            Gold.Instance.AddGold(gold, 0f);
            Debug.Log(gold);

        }
        else if (num == 6)//수험생냥이팩
        {
            int gold = 32000;
            gold += PurchaseItem(0, 9);
            gold += PurchaseItem(1, 12);
            gold += PurchaseItem(2, 5);
            Gold.Instance.AddGold(gold, 0f);
            Debug.Log(gold);

        }
        if(onSuccess != null) onSuccess();
        #if UNITY_ANDROID
        if(GPGSManager.Instance != null) GPGSManager.Instance.OnClickSave(null, null);
        #endif
    }

    private int PurchaseItem(int type, int num)
    {
        if (ShopDataManager.Instance.IsPurchased(type, num))
        {
            return ShopDataManager.Instance.GetPrice(type, num);
        }
        else
        {
            //OnBuy
            ShopDataManager.Instance.SetPossessionTrue(type, num);
            if (type == 0)
            {
                PlayerPrefs.SetInt("CurCat", num);
                ShopDataManager.Instance.curCat = num;
                PlayerAnimation.Instance.SetCatNum(num);
            }
            else if (type == 1)
            {
                PlayerPrefs.SetInt("CurBlock", num);
                ShopDataManager.Instance.curBlock = num;
                SpriteManager.Instance.SetBlockSkin(num);

            }
            else
            {
                PlayerPrefs.SetInt("CurBG", num);
                ShopDataManager.Instance.curBG = num;
                Background.Instance.SetSprite();

            }
            return 0;
        }
    }

    public void ShowPackagePanel(PurchaseItem item)
    {
        PopupManager.Instance.PopupActive(packagePanel, true);
        packagePanel.GetComponent<PackagePanel>().SetPackage(item);
        PopupManager.Instance.ShowBackground();
    }

    public void GetPackPurchased()
    {
        int pack = PlayerPrefs.GetInt("PackPurchase", 0);
        for (int i = 0; i < isPackPurchased.Length; i++)
        {
            isPackPurchased[i] = ((pack >> i) % 2) == 1;
        }
    }

    public void SavePackPurchased()
    {
        int pack = 0;
        for (int i = 0; i < isPackPurchased.Length; i++)
        {
            if (isPackPurchased[i]) pack += (1 << i);
        }
        PlayerPrefs.SetInt("PackPurchase", pack);
        PlayerPrefs.Save();
    }

    public void SetPackPurchased(int num)
    {
        isPackPurchased[num - packStartNum] = true;
        SavePackPurchased();
    }

    public void ResetPackPurchase()
    {
        PlayerPrefs.SetInt("PackPurchase", 0);
        PlayerPrefs.Save();
    }

    public void ShowPurchaseSuccessPanel()
    {
        PopupManager.Instance.PopupActive(purchaseSueccess, true);
    }

}
