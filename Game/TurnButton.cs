﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TurnButton : MonoBehaviour, IPointerDownHandler
{
    Image image;
    public Sprite[] sprites;
    public static TurnButton Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
        image = GetComponent<Image>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnClickTurn();
    }

    public void OnClickTurn()
    {
        if (Player.Instance.punchable)
        {
            // if (BlockItemSkill.Instance.isSuperMode)
            // {
            //     if (!Map.Instance.blocks[0].isTurnPos)
            //     {
            //         Player.Instance.Punch();
            //     }
            //     else
            //     {
            //         Player.Instance.Turn();
            //     }
            // }
            // else
            // {
            Player.Instance.Turn();
            Tutorial.Instance.CheckTurnPos();

            // }
        }
    }

    public void SetSprite(bool punch)
    {
        if(punch)
        {
            image.sprite = sprites[1];
        }
        else
        {
            image.sprite = sprites[0];
        }
    }


}