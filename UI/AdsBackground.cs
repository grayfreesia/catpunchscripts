﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsBackground : MonoBehaviour
{
    public GameObject back;

    public static AdsBackground Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void ShowBackground(bool on)
    {
        back.SetActive(on);
    }
}
