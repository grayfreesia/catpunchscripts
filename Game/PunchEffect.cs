﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchEffect : MonoBehaviour
{
    public Sprite sprite;
    private SpriteRenderer spriteRenderer;
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Effect());
    }

    IEnumerator Effect()
    {
        yield return new WaitForSeconds(.1f);
        spriteRenderer.sprite = sprite;
        yield return new WaitForSeconds(.1f);
        Destroy(gameObject);
    }
}
