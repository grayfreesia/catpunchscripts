﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class TimeManager : MonoBehaviour
{
    private string uri = "https://google.com";
    public string serverTime = "";
    private string lastFreeGiftTime;

    private float time = 0f;
    private int ss = 0;
    private int mm = 0;
    private int hh = 0;
    private TimeSpan span;
    public TimeSpan remain;
    public int wait = 0;
    private TimeSpan waitAmount = new TimeSpan(6, 0, 0);
    private bool isPaused = false;

    public static TimeManager Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void Start()
    {
        // GetServerTime();
        lastFreeGiftTime = PlayerPrefs.GetString("LastFreeGiftTime", "");
        wait = PlayerPrefs.GetInt("WaitAmount", 6);
        waitAmount = new TimeSpan(wait, 0, 0);
        if (lastFreeGiftTime == "")
        {
            lastFreeGiftTime = new DateTime(2020, 4, 1, 0, 0, 0).ToString();
        }

        GetServerTime(GetTimeCallBack);
    }

    void Update()
    {
        TimeSpend();
    }

    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            isPaused = true;
            /* 앱이 비활성화 되었을 때 처리 */
        }
        else
        {
            if (isPaused)
            {
                isPaused = false;
                /* 앱이 활성화 되었을 때 처리 */
                GetServerTime(GetTimeCallBack);
            }
        }
    }

    public void GetServerTime(Action callBack)
    {
        // if (w == null)
        // {
        //     w = StartCoroutine(GetServerTimeCR(callBack));
        // }

        StartCoroutine(GetServerTimeCR(callBack));
    }

    IEnumerator GetServerTimeCR(Action callBack)
    {
        UnityWebRequest webRequest = UnityWebRequest.Head(uri);
        yield return webRequest.SendWebRequest();
        // Debug.Log(webRequest);
        if (webRequest.isNetworkError)
        {
            Debug.Log("Error: " + webRequest.error);
            serverTime = DateTime.Now.ToString();
        }
        else
        {
            string response = webRequest.GetResponseHeader("date");
            // Debug.Log("Received: " + response);
            //Received: Fri, 24 Apr 2020 07:20:16 GMT
            // TimeParser(response);
            serverTime = response;
        }
        callBack();
        // yield return new WaitForSeconds(1f);
    }

    public void GetTimeCallBack()
    {
        span = DateTime.Parse(serverTime) - DateTime.Parse(lastFreeGiftTime);
        span = waitAmount - span;
    }

    private void TimeSpend()
    {
        time += Time.deltaTime;
        if (time >= 1f)
        {
            time -= 1f;
            ss++;
            if (ss >= 60)
            {
                ss = 0;
                mm++;
                if (mm >= 60)
                {
                    mm = 0;
                    if (hh < 10) hh++;
                }
            }
            // Debug.Log(remain +"d"+ TimeManager.Instance.span +"d"+ new TimeSpan(hh,mm,ss));
            remain = span - new TimeSpan(hh, mm, ss);
        }
    }
    public void SetLastTime()
    {
        GetServerTime(SetLastTimeCallBack);
    }

    public void SetLastTimeCallBack()
    {
        lastFreeGiftTime = serverTime;
        span = waitAmount;
        InitTimeCount();
        PlayerPrefs.SetString("LastFreeGiftTime", serverTime.ToString());
        // SetServerTime();
        // Debug.Log("set last time" + TimeManager.Instance.serverTime);
    }

    public void SetWaitAmount(int num)
    {
        if (num == 0)
        {
            waitAmount = new TimeSpan(0, 0, 10);
        }
        else if (num <= 6)
        {
            waitAmount = new TimeSpan(num, 0, 0);
        }
        PlayerPrefs.SetInt("WaitAmount", num);
        wait = num;

    }

    public void SetServerTime()
    {
        GetServerTime(GetTimeCallBack);
    }

    public void InitTimeCount()
    {
        ss = 0;
        mm = 0;
        hh = 0;
    }


}
