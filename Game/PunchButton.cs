﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PunchButton : MonoBehaviour, IPointerDownHandler
{
    public static PunchButton Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnClickPunch();
    }

    public void OnClickPunch()
    {
        if (Player.Instance.punchable)
        {
            // if (BlockItemSkill.Instance.isSuperMode)
            // {
            //     if (!Map.Instance.blocks[0].isTurnPos)
            //     {
            //         Player.Instance.Punch();
            //     }
            //     else
            //     {
            //         Player.Instance.Turn();
            //     }
            // }
            // else
            // {
            Player.Instance.Punch();
            Tutorial.Instance.CheckTurnPos();
            // }
        }
    }
}
