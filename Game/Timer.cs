﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    // private Slider slider;
    public TimerSlider slider;
    // private float time = 1f;
    private float totalTime = 10f;
    private float remainTime = 10f;
    private float add = 1f;
    private float reduce = 1f;
    private float reduceAdd = 0.01f;
    private float reduceMax = 4.5f;
    private float reduceStart = 1f;
    
    public bool isRunning = false;

    public static Timer Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
        // slider = GetComponent<Slider>();
        remainTime = totalTime;
        SetTimeDifficulty(1);
    }

    void Update()
    {
        ReduceTime();
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
        GameManager.OnEnd += OnEnd;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
        GameManager.OnEnd -= OnEnd;
    }
    
    public void OnStart()
    {
        totalTime = 10f;
        remainTime = 5f;
        slider.SetValue(.5f);
        // isIce = false;
        reduce = reduceStart;
        // isRunning = true;
    }

    public void OnEnd()
    {
        isRunning = false;
    }

    public void OnContinue()
    {
        remainTime = totalTime;
    }

    public void AddTime()
    {
        if(BlockItemSkill.Instance.isIce) return;
        remainTime += add;
        if(remainTime > totalTime)
        {
            remainTime = totalTime;
        }
        if(reduce < reduceMax)
        {
            reduce += reduceAdd * ((reduceMax- reduce) / reduceMax);
        }
    }

    private void ReduceTime()
    {
        if(!isRunning || BlockItemSkill.Instance.isIce) return;
        remainTime -= Time.deltaTime * reduce;
        slider.SetValue(remainTime/totalTime);
        if(remainTime <= 0)
        {
            TimeFail();
            remainTime = totalTime;
        }
    }

    private void TimeFail()
    {
        GameManager.Instance.FailGame();
    }

    // public void SetReduceAdder(float num)
    // {
    //     //default 0.2f
    //     reduceAdd = num;
    // }

    public void SetTimeDifficulty(int value)
    {
        switch(value)
        {
            case 0:
                reduceStart = 1f;
                reduceAdd = 0.01f;
                add = 1f;
                break;
            case 1:
                reduceStart = 1.2f;
                reduceAdd = 0.02f;
                add = 0.8f;
                break;
            case 2:
                reduceStart = 1.5f;
                reduceAdd = 0.03f;
                add = 0.6f;
                break;
            default:
                break;
        }
    }

    public void IceTimeImage(bool on)
    {
        slider.IceImage(on);
        if(on)
        {
            remainTime = totalTime;
            slider.SetValue(1f);
        }
    }
    
}
