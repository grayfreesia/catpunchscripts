﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageDropdownFontHelper : MonoBehaviour
{
    public Text text;
    public int num = 0;
    void Awake()
    {
        num = 0;
        text = GetComponentInChildren<Text>();
    }

    void Start()
    {
        num = transform.GetSiblingIndex();
        text.font = LanguageManager.Instance.fonts[num-1];
    }

}
