﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public static float size = .7f;//블록간격
    private const float blockSize = .35f;
    public GameObject blockPrefab;
    public List<Block> blocks = new List<Block>();
    private Vector2Int curPos;//마지막 생성한 위치
    private Vector2Int pos;//map 위치
    private int state = -1;
    private Vector3 origin;
    private float startDifficulty = .1f;
    private float difficulty = .1f;//0~1
    private int prev = 1;
    private Vector3 contPos;
    public bool dir = true; //for assist mode
    
    public static Map Instance;

    void Awake()
    {
        if (Instance == null) Instance = this;
        origin = transform.position;
        SetDifficulty(.4f);
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
        GameManager.OnEnd += OnEnd;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
        GameManager.OnEnd -= OnEnd;
    }

    public void OnStart()
    {
        curPos = Vector2Int.zero;
        pos = new Vector2Int(0, 0);
        transform.position = origin;
        state = -1;
        prev = 1;
        difficulty = startDifficulty;
        dir = true;
        
        for (int i = 0; i < blocks.Count; i++)
        {
            Destroy(blocks[i].gameObject);
        }
        blocks.Clear();
        for (int i = 0; i < 10; i++)
        {
            SpawnUp();
        }
        // iTween.Stop(gameObject);
        iTween.MoveFrom(gameObject, iTween.Hash("position", new Vector3(0, 1, 0), "time", .5f, "easetype", iTween.EaseType.easeOutBounce));
    }

    public void OnEnd()
    {
        // FailDrop();
    }

    private void SpawnBlock(int num)
    {
        //2 3 4
        //0 x 1
        switch (num)
        {
            case 0:
                curPos += new Vector2Int(-1, 0);
                state = 2;
                break;
            case 1:
                curPos += new Vector2Int(1, 0);
                state = 1;
                break;
            case 2:
                curPos += new Vector2Int(-1, 1);
                state = 2;
                break;
            case 3:
                curPos += new Vector2Int(0, 1);
                break;
            case 4:
                curPos += new Vector2Int(1, 1);
                state = 1;
                break;
            default:
                break;
        }

        Block newBlock = Instantiate(blockPrefab, transform).GetComponent<Block>();
        newBlock.transform.localScale = Vector3.one * size * blockSize;
        newBlock.pos = curPos;
        newBlock.transform.localPosition = (Vector2)curPos * size;
        blocks.Add(newBlock);

        //assist//mark
        BlockMarker.Instance.BlockTurnMarkCheck(newBlock, num);
    }

    public void SpawnRandom()
    {
        int r = 0;
        if (Random.Range(0f, 1f) < difficulty)
        {
            switch (state)
            {
                case -1://init state
                    r = 1;
                    state = 0;
                    break;
                case 0://all possible state
                    r = Random.Range(0, 5);
                    break;
                case 1://cant go left
                    r = Random.Range(1, 5);
                    break;
                case 2://cant go right
                    r = Random.Range(1, 5);
                    if (r == 1) r = 0;
                    break;
                default:
                    break;

            }
        }
        else
        {
            r = prev;
        }
        prev = r;
        SpawnBlock(r);
    }

    public void DestroyBlock()
    {
        if (blocks.Count == 0) return;

        Block toDestroy = blocks[0];
        blocks.RemoveAt(0);
        if (blocks.Count > 0)
        {
            if (blocks[0].pos.y != toDestroy.pos.y)//다음 블럭이 위에 있으면 전체가 한칸씩 내려옴
            {
                Move(Vector2Int.down);
                Player.Instance.pos += Vector2Int.up;
                if(blocks[0].pos.x == toDestroy.pos.x)
                {
                    //step animation
                    StepAnimation();
                }
            }
            if (blocks[0].pos.x != toDestroy.pos.x)//다음 블럭이 바로 위일땐 안움직임
            {
                if (Player.Instance.pos.x > toDestroy.pos.x)
                {
                    Move(Vector2Int.right);//플레이어가 왼쪽으로 움직이려면 맵은 오른쪽으로 움직여야함
                    Player.Instance.pos += Vector2Int.left;

                }
                else if (Player.Instance.pos.x < toDestroy.pos.x)
                {
                    Move(Vector2Int.left);
                    Player.Instance.pos += Vector2Int.right;

                }
            }
        }

        toDestroy.BlockDestroy();
        // Destroy(toDestroy.gameObject);
    }

    public void Move(Vector2Int dir)
    {
        // transform.Translate((Vector2)dir * size);
        pos += dir;
        Vector3 position = origin + new Vector3(pos.x, pos.y, 0) * size;
        iTween.MoveTo(gameObject, iTween.Hash("position", position, "time", .2f));
        //블럭생성해야함
        if (dir == Vector2Int.down)
        {
            SpawnUp();
        }
        else
        {
            Background.Instance.Move(dir);
        }
    }

    private void StepAnimation()
    {
        float step = Random.Range(-.5f, .5f);
        Vector3 position = origin + new Vector3(pos.x + step, pos.y, 0) * size;
        iTween.MoveTo(gameObject, iTween.Hash("position", position, "time", .2f));
        Background.Instance.Move(new Vector2(step, 0));
    }

    private void SpawnUp()
    {
        //한 줄 위로 올라갈때까지 생성
        int curY = curPos.y;
        int limit = 0;//희박하지만 무한루프 방지
        while (curPos.y == curY)
        {
            SpawnRandom();

            limit++;
            if (limit > 100)
            {
                SpawnBlock(2);
                break;
            }
        }
    }

    public Vector2Int GetNextBlockPos()
    {
        if (blocks.Count > 0)
        {
            return blocks[0].pos;
        }
        else
        {
            return Vector2Int.zero;
        }
    }

    public void FailDrop()
    {
        contPos = transform.position;
        iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + new Vector3(0, -10f, 0), "time", 2f));
    }

    public void ContinueUp()
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", contPos, "time", 1f));
    }

    public void AddDifficulty(float add)
    {
        if (difficulty < 1f && add > 0f)
        {
            difficulty += add;
        }
    }

    public void SetDifficulty(float value)
    {
        if (value <= 1.0f && value >= 0.1f)
        {
            startDifficulty = value;
            difficulty = value;
        }
    }

    
}
