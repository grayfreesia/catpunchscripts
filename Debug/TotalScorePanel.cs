﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalScorePanel : MonoBehaviour
{
    public Text text;
    private int total;

    void OnEnable()
    {
        total = PlayerPrefs.GetInt("TotalScore", 0);
        text.text = "총 점수 :" + total.ToString() + "\n 점수당 획득 골드 :" + ((float)Gold.Instance.GetGold()/total).ToString();
    }
}
