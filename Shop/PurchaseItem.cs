﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseItem : MonoBehaviour
{
    public Text packName;
    public Text buttonText;
    public Text amountText;
    public Text originCostText;
    public Text costText;
    public GameObject icon;
    public int num = 0;
    public bool isPackage;
    // public static int packStartNum = 4;
    private static string[] price = { "₩16,250", "₩5,900","₩17,000", "₩5,900","₩33,250","₩9,900",
                                     "₩1,200","₩5,900","₩12,000" };
    private static int[] goldAmount = { 200, 3000, 18000, 45000 };

    
    void Start()
    {
        // num = transform.GetSiblingIndex();
        OffItem();
    }

    void Update()
    {
        SetTimeText();
    }

    void OnEnable()
    {
        SetString();
        SetText();
    }

    private void SetString()
    {
        for(int i = 0; i < price.Length; i++)
        {
            price[i] = LanguageManager.GetString(5, i);
        }
    }

    public void OnClickPurchase()
    {
        if (num == 0)
        {
            //ads
            if (ShopTimeManager.Instance.remain > TimeSpan.Zero) return;
#if UNITY_EDITOR
            ShopTimeManager.Instance.SetLastTime();
            PurchaseManager.Instance.OnPurchase(0);
            PurchaseManager.Instance.ShowPurchaseSuccessPanel();
#else
            AdsBackground.Instance.ShowBackground(true);
            AdSystem.ShowAd(() =>
            {
                AdsBackground.Instance.ShowBackground(false);
                ShopTimeManager.Instance.SetLastTime();
                PurchaseManager.Instance.OnPurchase(0);
                PurchaseManager.Instance.ShowPurchaseSuccessPanel();

            });
#endif
        }
        else if (!isPackage)
        {
            //purchase
#if UNITY_EDITOR
            PurchaseManager.Instance.OnPurchase(num);
            PurchaseManager.Instance.ShowPurchaseSuccessPanel();

#else
            PurchaseManager.Instance.OnClickPurchase(num, ()=>
            {
                PurchaseManager.Instance.ShowPurchaseSuccessPanel();
            });
#endif

        }
        else
        {
            PurchaseManager.Instance.ShowPackagePanel(this);
        }

        // Gold.Instance.AddGold(goldAmount[num]);

    }

    public void SetTimeText()
    {
        if (num != 0) return;
        if (ShopTimeManager.Instance.remain <= TimeSpan.Zero)
        {
            icon.SetActive(true);
            buttonText.text = "";
        }
        else
        {
            icon.SetActive(false);
            buttonText.text = ShopTimeManager.Instance.remain.ToString();
        }
    }

    private void SetText()
    {
        if (num == 0) return;
        if (!isPackage)
        {
            LanguageManager.Instance.SetFont(buttonText);
            amountText.text = "X" + string.Format("{0:#,###0}", goldAmount[num]);
            buttonText.text = price[num + 5];
        }
        else
        {
            LanguageManager.Instance.SetFont(packName, originCostText, costText);
            packName.text = LanguageManager.GetString(4, 4 + (num - PurchaseManager.packStartNum) * 4);
            originCostText.text = price[(num - 4)*2];
            costText.text = price[(num - 4)*2+1];
        }
    }

    public void OffItem()
    {
        if (!isPackage) return;
        if (PurchaseManager.Instance.isPackPurchased[num - PurchaseManager.packStartNum])
        {
            gameObject.SetActive(false);
        }
    }
}
