﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource bgmSource;
    public AudioSource sfxSource;
    public AudioClip[] clips;

    public static SoundManager Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
        SetVolume(0, PlayerPrefs.GetFloat("BGM", 1f));
        SetVolume(1, PlayerPrefs.GetFloat("SFX", 1f));
    }

    public void Play(int num)
    {
        // float v = 1f;
        // if(num == 0 || num == 5) v = .5f;
        sfxSource.PlayOneShot(clips[num]);
    }

    public void SetVolume(int source, float value)
    {
        if(source == 0)
        {
            bgmSource.volume = value;
        }
        else
        {
            sfxSource.volume = value;
        }
    }
}
