﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardPanel : MonoBehaviour
{
    private Button[] buttons;
    public Text[] buttonTexts;
    private bool isFreeGiftOn = false;
    private bool isDebug = false;
    public GameObject freeGiftIcon;
    public Text remainTime;
    public static RewardPanel Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        buttons = GetComponentsInChildren<Button>();
    }

    void Update()
    {
        if (isFreeGiftOn) SetFreeGift();
    }

    void OnEnable()
    {
        if (DebugButtonShow.Instance != null && DebugButtonShow.Instance.rewardDebug) SetDebug();
        OnOffBanner();
        SetString();
        LanguageManager.OnChange += SetString;
    }

    void OnDisable()
    {
        LanguageManager.OnChange -= SetString;
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(buttonTexts);
        buttonTexts[0].text = LanguageManager.GetString(0, 1);
        buttonTexts[1].text = LanguageManager.GetString(0, 4);
        buttonTexts[2].text = LanguageManager.GetString(0, 5);
    }

    private void OnOffBanner()
    {
        if (isDebug || TimeManager.Instance.remain <= TimeSpan.Zero || UnityEngine.Random.Range(0, 10) == 0)
        {
            buttons[0].gameObject.SetActive(true);
            isFreeGiftOn = true;
            SetFreeGift();
        }
        else
        {
            buttons[0].gameObject.SetActive(false);
            isFreeGiftOn = false;
        }
        int bestScore = PlayerPrefs.GetInt("BestScore", 0);
        if (isDebug || Point.Instance.point > bestScore / 2 && UnityEngine.Random.Range(0, 2) == 0 && !GameManager.Instance.isContinued)
        {
            buttons[1].gameObject.SetActive(true);
        }
        else
        {
            buttons[1].gameObject.SetActive(false);
        }
        if (isDebug || UnityEngine.Random.Range(0, 10) == 0)
        {
            buttons[2].gameObject.SetActive(true);
            buttons[2].interactable = true;
        }
        else
        {
            buttons[2].gameObject.SetActive(false);
        }
    }

    private void SetFreeGift()
    {
        if (TimeManager.Instance.remain <= TimeSpan.Zero)
        {
            remainTime.gameObject.SetActive(false);
            freeGiftIcon.SetActive(true);
            buttons[0].interactable = true;
        }
        else
        {
            remainTime.gameObject.SetActive(true);
            freeGiftIcon.SetActive(false);
            buttons[0].interactable = false;
            remainTime.text = TimeManager.Instance.remain.ToString();
        }
    }

    public void OnClickFreeGift()
    {
        // buttons[0].interactable = false;
        buttons[0].gameObject.SetActive(false);
        isFreeGiftOn = false;
    }

    public void OnClickRewardGift()
    {
        // buttons[2].interactable = false;
        buttons[2].gameObject.SetActive(false);
    }

    public void SetDebug()
    {
        isDebug = true;
    }



}
