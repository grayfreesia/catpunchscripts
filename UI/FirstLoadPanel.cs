﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstLoadPanel : MonoBehaviour
{
    public GameObject loading;

    void Update()
    {
        loading.transform.Rotate(Vector3.forward * Time.deltaTime * -200f);
    }

}
