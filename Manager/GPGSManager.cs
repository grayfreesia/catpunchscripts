﻿#if UNITY_ANDROID
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.SavedGame;
using System;
// using System.Text;
// using Newtonsoft.Json;
// using System.IO;

public class GPGSManager : MonoBehaviour
{
    // public Text dataText;

    // private int data = 0;
    private bool isSaving;
    private const string FILE_NAME = "catpunch_save.bin";
    private Action onLoginFail;
    private Action onSaveFail;
    private Action onLoadFail;
    private Action onSaveSuccess;
    private Action onLoadSuccess;

    // public GameObject firstLoadPanel;
    // private Action onAuthenticateFail;
    public static GPGSManager Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void Start()
    {
        Init();
        SignIn((result) =>
        {
            if (result)
            {
                if (PlayerPrefs.GetInt("IsFirstLoad", 1) == 1)//1 : true, 0 : false
                {
                    
                    // firstLoadPanel.SetActive(true);
                    // OnClickLoad(()=>firstLoadPanel.SetActive(false), ()=>firstLoadPanel.SetActive(false));//첫 로그인시 로드
                    OnClickLoad(null, null);
                }
                else
                {
                    OnClickSave(null, null);
                }
            
            }
            else
            {

            }
        });
    }

    //초기화 
    public void Init()
    {
        PlayGamesClientConfiguration conf = new PlayGamesClientConfiguration.Builder()
        .EnableSavedGames()//저장기능
        .Build();
        PlayGamesPlatform.InitializeInstance(conf);
        PlayGamesPlatform.DebugLogEnabled = true;//debug
        PlayGamesPlatform.Activate();
    }


    //로그인 
    public void SignIn(System.Action<bool> onComplete)
    {
        Social.localUser.Authenticate(onComplete);
    }

    //로그아웃
    public void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    public void OnClickSave(Action onSaveSuccess, Action onSaveFail)
    {
        isSaving = true;
        this.onSaveSuccess = onSaveSuccess;
        this.onSaveFail = onSaveFail;
        OpenSavedGame(FILE_NAME);
    }

    public void OnClickLoad(Action onLoadSuccess, Action onLoadFail)
    {
        isSaving = false;
        this.onLoadSuccess = onLoadSuccess;
        this.onLoadFail = onLoadFail;
        OpenSavedGame(FILE_NAME);
    }

    //저장, 로드할 파일 열기
    public void OpenSavedGame(string filename)
    {
        if (Social.localUser.authenticated)
        {

            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened);
        }
        else
        {
            // if(onAuthenticateFail != null && SettingSaveLoad.Instance != null) onAuthenticateFail();
            Debug.Log("not authenticated");
        }
    }

    public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)//파일 열기 성공
        {
            Debug.Log("succ open");
            // handle reading or writing of saved game.
            if (isSaving)
            {
                SaveGame(game, SaveDataManager.GetSaveData(), TimeSpan.Zero);
            }
            else
            {
                LoadGameData(game);
            }
        }
        else
        {
            // handle error
            Debug.Log("fail open");
        }
    }

    //데이터 저장
    void SaveGame(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
        builder = builder
            .WithUpdatedPlayedTime(totalPlaytime)
            .WithUpdatedDescription("Saved game at " + DateTime.Now);
        // if (savedImage != null)
        // {
        //     // This assumes that savedImage is an instance of Texture2D
        //     // and that you have already called a function equivalent to
        //     // getScreenshot() to set savedImage
        //     // NOTE: see sample definition of getScreenshot() method below
        //     byte[] pngData = savedImage.EncodeToPNG();
        //     builder = builder.WithUpdatedPngCoverImage(pngData);
        // }
        SavedGameMetadataUpdate updatedMetadata = builder.Build();
        savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
    }

    public void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)//저장 성공
        {
            // handle reading or writing of saved game.
            Debug.Log("save success!");
            if (onSaveSuccess != null) onSaveSuccess();
        }
        else
        {
            // handle error
            if (onSaveFail != null) onSaveFail();
        }
    }


    //데이터 로드
    void LoadGameData(ISavedGameMetadata game)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    public void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)//로드 성공
        {
            // handle processing the byte array data
            Debug.Log("onLoadSucc1");
            if (onLoadSuccess != null) 
            {
                onLoadSuccess();
                Debug.Log("onLoadSucc2");
            }   
            SaveDataManager.Instance.LoadData(data);
        }
        else
        {
            // handle error
            Debug.Log("onLoadFail1");
            if (onLoadFail != null) 
            {
                onLoadFail();
                Debug.Log("onLoadFail2");
            }
        }
    }

    public void DebugDelete()
    {
        DeleteGameData(FILE_NAME);
    }

    //데이터 삭제
    void DeleteGameData(string filename)
    {
        // Open the file to get the metadata.
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, DeleteSavedGame);
    }

    public void DeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.Delete(game);
        }
        else
        {
            // handle error
        }
    }

    public bool IsAuthenticated()
    {
        return Social.localUser.authenticated;
    }

    // public void SetCallbacks(params Action[] actions)
    // {
    //     if (actions.Length > 0) onLoginFail = actions[0];
    //     if (actions.Length > 1) onSaveFail = actions[1];
    //     if (actions.Length > 2) onLoadFail = actions[2];
    //     if (actions.Length > 3) onSaveSuccess = actions[3];
    //     if (actions.Length > 4) onAuthenticateFail = actions[4];
    // }

    public void ShowLeaderboardUI()
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(PlayerPrefs.GetInt("BestScore", 0), GPGSIds.leaderboard_cat_punch_ranking, (bool success) =>
                {
                    // handle success or failure
                    if (success) Social.ShowLeaderboardUI();
                    else Debug.Log("report score fail");
                });
        }
        else
        {
            SignIn((result) =>
            {
                if (result)
                {
                    if (PlayerPrefs.GetInt("IsFirstLoad", 1) == 1)//1 : true, 0 : false
                    {
                        // firstLoadPanel.SetActive(true);
                        // OnClickLoad(()=>firstLoadPanel.SetActive(false),()=>firstLoadPanel.SetActive(false));//첫 로그인시 로드
                        OnClickLoad(null, null);
                    }
                    else
                    {
                        Social.ReportScore(PlayerPrefs.GetInt("BestScore", 0), GPGSIds.leaderboard_cat_punch_ranking, (bool success) =>
                        {
                            // handle success or failure
                            if (success) Social.ShowLeaderboardUI();
                            else Debug.Log("report score fail");
                        });
                    }
                }
                else
                {
                    Debug.Log("Login fail");
                }
            });
        }


    }



    public void OnSavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
    {
        if (status == SelectUIStatus.SavedGameSelected)
        {
            // handle selected game save
            Debug.Log("save select success");
        }
        else
        {
            // handle cancel or error
            Debug.Log("save select fail");

        }
    }


    public void ShowSelectUI()
    {
        uint maxNumToDisplay = 5;
        bool allowCreateNew = false;
        bool allowDelete = true;
        Debug.Log("show select ui");
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ShowSelectSavedGameUI("Select saved game",
            maxNumToDisplay,
            allowCreateNew,
            allowDelete,
            OnSavedGameSelected);
        Debug.Log("show select ui end");

    }

    // public Texture2D getScreenshot()
    // {
    //     // Create a 2D texture that is 1024x700 pixels from which the PNG will be
    //     // extracted
    //     Texture2D screenShot = new Texture2D(1024, 700);

    //     // Takes the screenshot from top left hand corner of screen and maps to top
    //     // left hand corner of screenShot texture
    //     screenShot.ReadPixels(
    //         new Rect(0, 0, Screen.width, (Screen.width / 1024) * 700), 0, 0);
    //     return screenShot;
    // }

    //https://github.com/playgameservices/play-games-plugin-for-unity

}
#endif