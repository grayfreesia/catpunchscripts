﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartPunch : MonoBehaviour
{
    // Image image;
    Text text;
    Outline[] outlines;
    void Awake()
    {
        // image = GetComponent<Image>();
        text = GetComponentInChildren<Text>();
        outlines = GetComponentsInChildren<Outline>();
    }

    void Start()
    {
        StartCoroutine("Blink");
    }

    public void OnClickStart()
    {
        transform.parent.GetChild(0).GetComponent<StartLogo>().MoveUp();
        GameManager.Instance.StartGame();
        gameObject.SetActive(false);
    }

    IEnumerator Blink()
    {
        float a = -.02f;
        while (true)
        {
            text.color += new Color(0, 0, 0, a);
            foreach(Outline outline in outlines)
            {
                outline.effectColor += new Color(0, 0, 0, a/2f);
            }
            if (text.color.a >= .99f || text.color.a <= 0f)
            {
                a = -a;
            }
            yield return null;
        }
    }

}
