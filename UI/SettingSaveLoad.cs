﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingSaveLoad : MonoBehaviour
{
    public GameObject loginContainer;
    public GameObject saveloadContainer;

    public GameObject savelaodPanel;

    public Text save;
    public Text load;

    // public int num;
    public static SettingSaveLoad Instance;

    void Awake()
    {
        if (Instance == null) Instance = this;
        // GPGSManager.Instance.SetCallbacks(OnLoginFail, OnSaveFail, OnLoadFail, OnSaveSuccess, OnAuthenticateFail);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            ShowSaveLoadPanel(-1);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            OnSaveFail();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            OnLoadFail();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            OnSaveSuccess();
        }

    }

    void OnEnable()
    {
        LanguageManager.OnChange += SetString;
        #if UNITY_ANDROID
        if (GPGSManager.Instance.IsAuthenticated())
        {
            loginContainer.SetActive(false);
            saveloadContainer.SetActive(true);
        }
        else
        {
            loginContainer.SetActive(true);
            saveloadContainer.SetActive(false);
        }
        #endif

        SetString();
    }

    void OnDisable()
    {
        LanguageManager.OnChange -= SetString;
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(save, load);
        save.text = LanguageManager.GetString(0, 23);
        load.text = LanguageManager.GetString(0, 24);
    }

    public void OnClickLogin()
    {
        Debug.Log("login..");
        #if UNITY_ANDROID
        GPGSManager.Instance.Init();
        GPGSManager.Instance.SignIn((result) =>
        {
            if (result)
            {
                Debug.Log("login success");
                if (PlayerPrefs.GetInt("IsFirstLoad", 1) == 1)//1 : true, 0 : false
                {
                    
                    OnClickLoad();//첫 로그인시 로드
                }

                loginContainer.SetActive(false);
                saveloadContainer.SetActive(true);
            }
            else
            {
                Debug.Log("login fail");
                OnLoginFail();
                //test
                // loginContainer.SetActive(false);
                // saveloadContainer.SetActive(true);
            }
        });
        #endif
    }

    public void OnClickSave()
    {
        ShowSaveLoadPanel(-1);
        #if UNITY_ANDROID
        GPGSManager.Instance.OnClickSave(OnSaveSuccess, OnSaveFail);
        #endif
    }

    public void OnClickLoad()
    {
        // ShowSaveLoadPanel(-1);
        #if UNITY_ANDROID
        GPGSManager.Instance.OnClickLoad(null, null);
        #endif
    }

    public void ShowSaveLoadPanel(int value)
    {
        PopupManager.Instance.PopupActive(savelaodPanel, true);
        SaveLoadPanel.Instance.SetState(value);
    }

    public void OnLoginFail()
    {
        ShowSaveLoadPanel(0);
    }

    public void OnSaveFail()
    {
        ShowSaveLoadPanel(1);
        // SaveLoadPanel.Instance.SetState(1);
    }

    public void OnLoadFail()
    {
        // SaveLoadPanel.Instance.SetState(2);
        ShowSaveLoadPanel(2);
    }

    public void OnSaveSuccess()
    {
        // SaveLoadPanel.Instance.SetState(3);
        ShowSaveLoadPanel(3);
    }

    public void OnAuthenticateFail()
    {
        // SaveLoadPanel.Instance.SetState(0);
        ShowSaveLoadPanel(0);
    }

}
