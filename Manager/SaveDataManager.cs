﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class SaveData
{
    public int bestScore;
    public int gold;
    public int packPurchase;
    public int catPossession;
    public int blockPossession;
    public int bgPossession;
    public int curCat;
    public int curBlock;
    public int curBG;
    public int missionNum;
    public int waitAmount;
    public string lastFreeGiftTime;
    public string lastShopGiftTime;
}

public class SaveDataManager : MonoBehaviour
{
    public GameObject gpgs;
    public GameObject loadingPanel;
    private static SaveData saveData = new SaveData();
    public static SaveDataManager Instance;

    void Awake()
    {
        if(Instance == null) Instance = this;

        #if UNITY_ANDROID
        gpgs.SetActive(true);
        #else
        gpgs.SetActive(false);
        #endif
    }

    public static byte[] GetSaveData()
    {
        InitSaveData();
        return ObjectToByteArraySerialize(saveData);
    }

    private static void InitSaveData()
    {
        saveData.bestScore = PlayerPrefs.GetInt("BestScore", 0);
        saveData.gold = PlayerPrefs.GetInt("Gold", 0);
        saveData.packPurchase = PlayerPrefs.GetInt("PackPurchase", 0);
        saveData.catPossession = PlayerPrefs.GetInt("CatPossession", 1);
        saveData.blockPossession = PlayerPrefs.GetInt("BlockPossession", 1);
        saveData.bgPossession = PlayerPrefs.GetInt("BgPossession", 1);
        saveData.curCat = PlayerPrefs.GetInt("CurCat", 0);
        saveData.curBlock = PlayerPrefs.GetInt("CurBlock", 0);
        saveData.curBG = PlayerPrefs.GetInt("CurBG", 0);
        saveData.missionNum = PlayerPrefs.GetInt("MissionNum", 1);
        saveData.waitAmount = PlayerPrefs.GetInt("WaitAmount", 6);
        saveData.lastFreeGiftTime = PlayerPrefs.GetString("LastFreeGiftTime", "");
        saveData.lastShopGiftTime = PlayerPrefs.GetString("LastShopGiftTime", "");
    }

    public static byte[] ObjectToByteArraySerialize(object obj)
    {
        using (var memoryStream = new MemoryStream())
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(memoryStream, obj);
            memoryStream.Flush();
            memoryStream.Position = 0;

            return memoryStream.ToArray();
        }
    }

    public void LoadData(byte[] bytes)
    {
        
        PlayerPrefs.SetInt("IsFirstLoad", 0);
        Debug.Log("bytes leng : " + bytes.Length);
        if(bytes.Length == 0) 
        {
            return;
        }
        saveData = Deserialize<SaveData>(bytes);
        InitLoadData();

        StartCoroutine("ShowLoading");

    }

    private static void InitLoadData()
    {
        PlayerPrefs.SetInt("BestScore", saveData.bestScore);
        PlayerPrefs.SetInt("Gold", saveData.gold);
        PlayerPrefs.SetInt("PackPurchase", saveData.packPurchase);
        PlayerPrefs.SetInt("CatPossession", saveData.catPossession);
        PlayerPrefs.SetInt("BlockPossession", saveData.blockPossession);
        PlayerPrefs.SetInt("BgPossession", saveData.bgPossession);
        PlayerPrefs.SetInt("CurCat", saveData.curCat);
        PlayerPrefs.SetInt("CurBlock", saveData.curBlock);
        PlayerPrefs.SetInt("CurBG", saveData.curBG);
        PlayerPrefs.SetInt("MissionNum", saveData.missionNum);
        PlayerPrefs.SetInt("WaitAmount", saveData.waitAmount);
        PlayerPrefs.SetString("LastFreeGiftTime", saveData.lastFreeGiftTime);
        PlayerPrefs.SetString("LastShopGiftTime", saveData.lastShopGiftTime);
        // if(saveData.bestScore != 0) return true;
        // if(saveData.gold != 0) return true;
        // if(saveData.packPurchase != 0) return true;
        // if(saveData.catPossession != 1) return true;
        // if(saveData.blockPossession != 1) return true;
        // if(saveData.bgPossession != 1) return true;
        // if(saveData.curCat != 0) return true;
        // if(saveData.curBlock != 0) return true;
        // if(saveData.curBG != 0) return true;
        // if(saveData.missionNum != 0) return true;

        // return false;
    }

    public static T Deserialize<T>(byte[] byteData)
    {
        using (var stream = new MemoryStream(byteData))
        {
            var formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }


    IEnumerator ShowLoading()
    {
        loadingPanel.SetActive(true);
        yield return new WaitForSeconds(2f);
        loadingPanel.SetActive(false);
        SceneManager.LoadScene("Main");
    }




    // 출처: https://teddy.tistory.com/23 [Teddy Games]
}
