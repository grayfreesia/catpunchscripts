﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    private bool isTutorial = false;
    private bool isPunch = false;
    public GameObject punch;
    public GameObject turn;
    public GameObject touchEffectPrefab;
    private GameObject touchEffect;
    public static Tutorial Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        isTutorial = PlayerPrefs.GetInt("IsTutorial", 1) == 1 ? true : false;
        if (isTutorial)
        {
            PlayerPrefs.SetInt("IsTutorial", 0);
            PunchTutorial();
        }
    }

    public void CheckTurnPos()
    {
        if (!isTutorial) return;
        if (Map.Instance.blocks.Count > 0 && Map.Instance.blocks[0].isTurnPos)
        {
            TurnTutorial();
        }
        else if(!isPunch && Map.Instance.blocks.Count > 0 && !Map.Instance.blocks[0].isTurnPos)
        {
            EndTutorial();
        }
    }

    public void DebugTutorial()
    {
        if (Map.Instance.blocks.Count > 0 && !Map.Instance.blocks[0].isTurnPos)
        {
            isTutorial = true;
            isPunch = true;
            PunchTutorial();
        }
    }

    public void PunchTutorial()
    {
        isTutorial = true;
        isPunch = true;
        turn.GetComponent<Image>().raycastTarget = false;
        turn.transform.GetChild(0).GetComponent<Image>().raycastTarget = false;
        if(touchEffect == null)touchEffect = Instantiate(touchEffectPrefab, punch.transform);
        touchEffect.transform.localPosition = Vector2.up * 100f;
    }

    public void TurnTutorial()
    {
        isPunch = false;
        turn.GetComponent<Image>().raycastTarget = true;
        turn.transform.GetChild(0).GetComponent<Image>().raycastTarget = true;

        punch.GetComponent<Image>().raycastTarget = false;
        punch.transform.GetChild(0).GetComponent<Image>().raycastTarget = false;

        touchEffect.transform.SetParent(turn.transform);
        touchEffect.transform.localPosition = Vector2.up * 100f;
    }

    public void EndTutorial()
    {
        turn.GetComponent<Image>().raycastTarget = true;
        turn.transform.GetChild(0).GetComponent<Image>().raycastTarget = true;

        punch.GetComponent<Image>().raycastTarget = true;
        punch.transform.GetChild(0).GetComponent<Image>().raycastTarget = true;

        Destroy(touchEffect);
        isTutorial = false;
    }


}
