﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectHelper : MonoBehaviour
{
    ScrollRect scrollRect;
    void Awake()
    {
        scrollRect = GetComponent<ScrollRect>();
    }

    void Start()
    {
        ResetPos();
    }

    // void OnEnable()
    // {
    //     // Invoke("ResetPos", .01f);
    // }

    void ResetPos()
    {
        scrollRect.verticalNormalizedPosition = 1f;
    }

}
