﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanel : MonoBehaviour
{
    public GameObject pause;

    private bool isPaused;

    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            isPaused = true;
            /* 앱이 비활성화 되었을 때 처리 */
        }
        else
        {
            if (isPaused)
            {
                isPaused = false;
                /* 앱이 활성화 되었을 때 처리 */
                if(Timer.Instance.isRunning)
                {
                    PausePanelOn();
                }
            }
        }
    }
    
    public void PausePanelOn()
    {
        pause.SetActive(true);
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        if(Timer.Instance != null && Timer.Instance.isRunning) Timer.Instance.isRunning = false;

    }

    public void PausePanelOff()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
        pause.SetActive(false);
        BackButtonManager.Instance.OnClose();
        if(Timer.Instance != null && !Timer.Instance.isRunning) 
        {
            // Time.timeScale = 1f;
            TimeManager.Instance.SetServerTime();
            // Timer.Instance.isRunning = false;
        }
    }
}
