﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialEffect : MonoBehaviour
{
    private Image image;
    public Sprite[] sprites;
    void Awake()
    {
        image = GetComponent<Image>();
        StartCoroutine(Animate());
    }

    IEnumerator Animate()
    {
        int n = 0;
        while(true)
        {
            image.sprite = sprites[n++%sprites.Length];
            yield return new WaitForSeconds(.5f);
        }
    }
}
