﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockItemSkill : MonoBehaviour
{
    private Coroutine auto;
    private int autoPunchRemain = 20;
    private float iceTime = 0f;
    public bool isIce = false;
    public GameObject autoPunchEffect;
    // public bool isSuperMode = false;
    // private float superTime = 5f;

    public static BlockItemSkill Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void Update()
    {
        // SuperTimeReduce();
        IceTimeReduce();
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
        GameManager.OnEnd += OnEnd;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
        GameManager.OnEnd -= OnEnd;
    }

    public void OnStart()
    {
        isIce = false;
        // isSuperMode = false;
    }

    public void OnEnd()
    {
        if (auto != null) StopCoroutine(auto);
        autoPunchRemain = 0;
    }

    public void AutoPunch()
    {
        autoPunchRemain = 20;
        if (Player.Instance.punchable)
        {
            auto = StartCoroutine("AutoPunchCR");
        }
    }
    IEnumerator AutoPunchCR()
    {
        Player.Instance.punchable = false;
        autoPunchEffect.SetActive(true);
        SoundManager.Instance.Play(6);
        while (autoPunchRemain > 0)
        {
            if (Timer.Instance.isRunning)
            {
                if (!Map.Instance.blocks[0].isTurnPos)
                {
                    Player.Instance.Punch();
                }
                else
                {
                    Player.Instance.Turn();
                }
                autoPunchRemain--;
            }


            yield return new WaitForSeconds(.1f);
        }
        autoPunchEffect.SetActive(false);
        Player.Instance.punchable = true;
    }

    public void IceTime()
    {
        isIce = true;
        iceTime = 5f;
        Timer.Instance.IceTimeImage(true);
        SoundManager.Instance.Play(5);
    }

    private void IceTimeReduce()
    {
        if (!isIce || !Timer.Instance.isRunning) return;
        iceTime -= Time.deltaTime;
        if (iceTime <= 0f)
        {
            IceTimeEnd();
        }
    }

    private void IceTimeEnd()
    {
        isIce = false;
        Timer.Instance.IceTimeImage(false);
    }

    public float GetIceTimeRatio()
    {
        return iceTime / 5f;
    }

    public void GoldBonus()
    {
        StartCoroutine(GoldBonusCR());
    }

    IEnumerator GoldBonusCR()
    {
        SoundManager.Instance.Play(4);
        List<Block> b = new List<Block>();
        foreach (Block block in Map.Instance.blocks)
        {
            b.Add(block);
        }

        foreach (Block block in b)
        {
            if (block != null) block.ChangeToGold();
            yield return new WaitForSeconds(.1f);
        }
    }

    // public void EnergyDrink()
    // {
    //     Timer.Instance.EnergyDrink();
    // }

    // public void SuperMode()
    // {
    //     isSuperMode = true;
    //     superTime = 5f;
    //     TurnButton.Instance.SetSprite(false);
    // }

    // private void SuperTimeReduce()
    // {
    //     if(!isSuperMode) return;
    //     superTime -= Time.deltaTime;
    //     if(superTime <= 0f)
    //     {
    //         SuperModeEnd();
    //     }
    // }

    // private void SuperModeEnd()
    // {
    //     isSuperMode = false;
    //     TurnButton.Instance.SetSprite(true);
    // }

}
