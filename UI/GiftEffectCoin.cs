﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftEffectCoin : MonoBehaviour
{
    private float waitTime = .5f;
    private float moveTime = .5f;
    private float scaleTime = .2f;
    private float scaleAmount = 1.2f;
    private Vector3 originScale;
    private Vector2 StartPos;
    RectTransform rectTransform;
    void Start()
    {
        originScale = transform.localScale;

        rectTransform = GetComponent<RectTransform>();
        // rectTransform.anchoredPosition = new Vector2(Random.Range(50f, 350f), -Random.Range(350f, 800f));
        rectTransform.anchoredPosition = StartPos;
        Invoke("Move", waitTime);
        Invoke("ScaleUp", waitTime + moveTime);
        // Invoke("ScaleDown", waitTime + moveTime + scaleTime/2f);
        Destroy(gameObject, waitTime + moveTime + scaleTime);
    }

    public void SetStartPos(Vector2 start, bool isWait)
    {
        StartPos = start;
        if(!isWait) waitTime = 0f;
    }

    public void Move()
    {
        Vector2 curPosion = rectTransform.anchoredPosition;
        Vector2 desPosion = new Vector2(-100f, 0f);

        // from 과 to 사이의 보간 값을 OnUIMove()의 매개변수로 전달한다.
        iTween.ValueTo(gameObject, iTween.Hash(
            "time", moveTime,
            "from", curPosion,
            "to", desPosion,
            "onupdate", "OnUIMove",
            "easetype", iTween.EaseType.easeInCubic
            ));
    }

    private void OnUIMove(Vector2 targetPosion)
    {
        // UI 의 좌표 이동은 anchoredPosion 으로 한다.
        rectTransform.anchoredPosition = targetPosion;
    }

    public void ScaleUp()
    {
 
        iTween.ScaleTo(gameObject, iTween.Hash(
            "time", scaleTime,
            "scale", originScale * scaleAmount,
            "easetype", iTween.EaseType.easeOutBack
        ));
        
    }

}
