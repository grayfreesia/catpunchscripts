﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardButton : MonoBehaviour
{
    public void OnClickLeaderboard()
    {
        #if UNITY_ANDROID
        if(GPGSManager.Instance != null)
        {
            GPGSManager.Instance.ShowLeaderboardUI();
        }
        #endif
    }
}
