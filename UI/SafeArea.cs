﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class SafeArea : MonoBehaviour
{

    // RectTransform rectTransform;
    // RectTransform canvasRectTransform;
    // int isDebug = 0;
    // Rect prev = Rect.zero;


    RectTransform Panel;
    Rect LastSafeArea = new Rect(0, 0, 0, 0);

    // public Text message;

    void Awake()
    {
        Panel = GetComponent<RectTransform>();
        Refresh();
    }

    void Update()
    {
        Refresh();
    }

    void Refresh()
    {
        Rect safeArea = GetSafeArea();

        if (safeArea != LastSafeArea)
            ApplySafeArea(safeArea);
    }

    Rect GetSafeArea()
    {
        return Screen.safeArea;
    }

    void ApplySafeArea(Rect r)
    {
        Debug.Log("Apply");
        LastSafeArea = r;

        Vector2 anchorMin = r.position;
        Vector2 anchorMax = r.position + r.size;
        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;
        Panel.anchorMin = anchorMin;
        Panel.anchorMax = anchorMax;
    }
    // void Awake()
    // {
    //     rectTransform = GetComponent<RectTransform>();
    //     canvasRectTransform = transform.parent.GetComponent<RectTransform>();
    //     isDebug = PlayerPrefs.GetInt("IsDebugSafeArea", 0);
    //     ApplySafeArea();
    // }

    // void Update()
    // {
    //     isDebug = PlayerPrefs.GetInt("IsDebugSafeArea", 0);
    //     ApplySafeArea();
    // }

    // public void ApplySafeArea()
    // {
    //     Rect r = Screen.safeArea;
        
    //     if (isDebug == 1)
    //     {
    //         r = new Rect(r.x, r.y, r.width, r.height * .9f);
    //     }
    //     Debug.Log("safearea : "+r );
    //     if(r == prev) return;
    //     prev = r;
    //     // Rect r = new Rect(0, 200, 1440, 2760);
    //     // Debug.Log(r);
    //     //have to scaler scale with width
    //     float ratioX = transform.parent.GetComponent<CanvasScaler>().referenceResolution.x / canvasRectTransform.sizeDelta.x;
    //     // float ratioY = transform.parent.GetComponent<CanvasScaler>().referenceResolution.y / canvasRectTransform.sizeDelta.y;
    //     // Debug.Log(ratioX);
    //     Vector2 newSize = new Vector2(r.width, r.height) * ratioX;
    //     Vector2 newPos = new Vector2(r.width * .5f + r.x, r.height * .5f + r.y) * ratioX;
    //     // Debug.Log(r);
    //     rectTransform.sizeDelta = newSize;
    //     rectTransform.anchoredPosition = newPos;


    // }
}
