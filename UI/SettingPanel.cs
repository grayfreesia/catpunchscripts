﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanel : MonoBehaviour
{
    public GameObject panel;
    public SettingSlider bgm;
    public SettingSlider sfx;
    private float bgmValue = 0f;
    private float sfxValue = 0f;
    private bool isSettingOn = false;
    public Text langText;
    private string[] langNames;
    
    void Awake()
    {
        langNames = LanguageManager.Instance.GetLangNames();
    }

    void Update()
    {
        if (isSettingOn)
        {
            bgmValue = bgm.GetValue();
            sfxValue = sfx.GetValue();
            SoundManager.Instance.SetVolume(0, bgmValue);
            SoundManager.Instance.SetVolume(1, sfxValue);
        }
    }

    public void OnClickSetting()
    {
        isSettingOn = true;
        // panel.SetActive(true);
        PopupManager.Instance.PopupActive(panel, true);
        if (DebugButtonShow.Instance != null)
        {
            DebugButtonShow.Instance.DebugButtonOnOff(true);
        }

        bgmValue = PlayerPrefs.GetFloat("BGM", 1f);
        sfxValue = PlayerPrefs.GetFloat("SFX", 1f);
        bgm.SetValue(bgmValue);
        sfx.SetValue(sfxValue);
        langText.text = langNames[LanguageManager.Instance.num];
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
    }

    public void OnClickExit()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
        LanguageManager.Instance.SaveLangSetting();
    }

    public void OnBack()
    {
        isSettingOn = false;
        bgmValue = bgm.GetValue();
        sfxValue = sfx.GetValue();
        PlayerPrefs.SetFloat("BGM", bgmValue);
        PlayerPrefs.SetFloat("SFX", sfxValue);
        PlayerPrefs.Save();
        // SoundManager.Instance.SetVolume(0, bgmValue);
        // SoundManager.Instance.SetVolume(1, sfxValue);
        BackButtonManager.Instance.OnClose();


        // panel.SetActive(false);
        PopupManager.Instance.PopupActive(panel, false);

        if (DebugButtonShow.Instance != null)
        {
            DebugButtonShow.Instance.DebugButtonOnOff(false);
        }
    }

    public void OnClickLanguage()
    {
        LanguageManager.Instance.OnChangeLanguage();
        langText.text = langNames[LanguageManager.Instance.num];
    }

}
