﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopDataManager : MonoBehaviour
{
    public int curCat = 0;
    public int curBlock = 0;
    public int curBG = 0;

    public ShopItem curCatItem;
    public ShopItem curBlockItem;
    public ShopItem curBGItem;

    private int catPossession = 1;//32개까지 가능
    private int blockPossession = 1;
    private int bgPossession = 1;

    private int[] catPrices = {0, 3000, 3000, 3000, 5000, 5500, 6000, 8000, 9000, 10000, 10000, 10000, 10000, 10000};
    private int[] blockPrices = { 0, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 6500, 6500, 6500, 6500};
    private int[] BGPrices = { 0, 5000, 8000, 10000, 12000, 18000, 18000};

    private string[] catNames = {"하얀냥", "치즈냥", "삼색냥", "턱시도냥", "태권냥",
                                 "복싱냥", "요원냥", "달콤냥", "어흥냥", "수험생냥",
                                  "냥1", "냥2", "냥3", "냥4"};
    private string[] blockNames = {"박스", "풍선", "밥그릇", "휴지", "화분",
                                "털실공", "사리면", "사탕", "머그컵", "프레첼",
                                 "고기", "하트", "수험서", "블럭1", "블럭2",
                                 "블럭3", "블럭4",};
    private string[] BGNames = {"집사방", "산길", "체육관", "정글", "하트",
                                 "도시", "배경1"};

    private int[] catLock = {0, 0, 0, 0, 0, 0, 0, 300, 350, 400, -400, -800, -1400, -1800};
    private int[] blockLock = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 300, 350, 400, -200, -600, -1200, -1600};
    private int[] bgLock = {0, 0, 0, 0, 0, 0, -1000};

    public ItemPanel itemPanel;
    public GameObject noMoneyPanel;
    private ShopBase[] shopBases;

    public static ShopDataManager Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        // SetLanguage();
        InitData();
    }

    void Start()
    {
        SetLanguage();
    }

    void InitData()
    {
        curCat = PlayerPrefs.GetInt("CurCat", 0);
        curBlock = PlayerPrefs.GetInt("CurBlock", 0);
        curBG = PlayerPrefs.GetInt("CurBG", 0);
        catPossession = PlayerPrefs.GetInt("CatPossession", 1);
        blockPossession = PlayerPrefs.GetInt("BlockPossession", 1);
        bgPossession = PlayerPrefs.GetInt("BgPossession", 1);
    }

    void OnEnable()
    {
        LanguageManager.OnChange += SetLanguage;
    }

    void OnDisable()
    {
        LanguageManager.OnChange -= SetLanguage;
    }

    private void SetLanguage()
    {
        for(int i = 0; i < catNames.Length; i++)
        {
            catNames[i] = LanguageManager.GetString(1, i+1);
        }
        for(int i = 0; i < blockNames.Length; i++)
        {
            blockNames[i] = LanguageManager.GetString(2, i+1);
        }
        for(int i = 0; i < BGNames.Length; i++)
        {
            BGNames[i] = LanguageManager.GetString(3, i+1);
        }
        
    }

    public int GetItemLength(int type)
    {
        if (type == 0)
        {
            return catPrices.Length;
        }
        else if (type == 1)
        {
            return blockPrices.Length;
        }
        else
        {
            return BGPrices.Length;
        }
    }

    public ShopItem GetCurItem(int type)
    {
        if (type == 0)
        {
            return curCatItem;
        }
        else if (type == 1)
        {
            return curBlockItem;
        }
        else
        {
            return curBGItem;
        }
    }

    public bool IsSelected(int type, int num)
    {
        if (type == 0)
        {
            return curCat == num;
        }
        else if (type == 1)
        {
            return curBlock == num;
        }
        else
        {
            return curBG == num;
        }
    }

    public int GetPrice(int type, int num)
    {
        if (type == 0)
        {
            return catPrices[num];
        }
        else if (type == 1)
        {
            return blockPrices[num];
        }
        else
        {
            return BGPrices[num];
        }
    }

    public bool IsPurchased(int type, int num)
    {
        if (type == 0)
        {
            return (catPossession >> num) % 2 == 1;
        }
        else if (type == 1)
        {
            return (blockPossession >> num) % 2 == 1;

        }
        else
        {
            return (bgPossession >> num) % 2 == 1;
        }
    }

    public int IsLock(int type, int num)
    {
        if (type == 0)
        {
            if(catLock[num] < 0) return catLock[num];
            if(PlayerPrefs.GetInt("BestScore", 0) < catLock[num])
            {
                return catLock[num];
            }
            else
            {
                return 0;
            }
        }
        else if (type == 1)
        {
            if(blockLock[num] < 0) return blockLock[num];

            if(PlayerPrefs.GetInt("BestScore", 0) < blockLock[num])
            {
                return blockLock[num];
            }
            else
            {
                return 0;
            }
        }
        else if (type == 2)
        {
            if(bgLock[num] < 0) return bgLock[num];

            if(PlayerPrefs.GetInt("BestScore", 0) < blockLock[num])
            {
                return bgLock[num];
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    // public ShopItem GetShopItem(int type, int num)
    // {
    //     ShopPanel.Instance.panels[type].Get
    // }

    public void SetCurrentItem(int type, int num, ShopItem item)
    {
        if (type == 0)
        {
            curCat = num;
            PlayerPrefs.SetInt("CurCat", num);
            curCatItem = item;
            PlayerAnimation.Instance.SetCatNum(num);
        }
        else if (type == 1)
        {
            curBlock = num;
            PlayerPrefs.SetInt("CurBlock", num);
            curBlockItem = item;
            SpriteManager.Instance.SetBlockSkin(num);
        }
        else
        {
            curBG = num;
            PlayerPrefs.SetInt("CurBG", num);
            curBGItem = item;
            Background.Instance.SetSprite();
        }
        PlayerPrefs.Save();
    }

    public void SetPossessionTrue(int type, int num)
    {
        if (type == 0)
        {
            if ((catPossession >> num) % 2 == 1) return;
            catPossession += 1 << num;
            PlayerPrefs.SetInt("CatPossession", catPossession);
        }
        else if (type == 1)
        {
            if ((blockPossession >> num) % 2 == 1) return;
            blockPossession += 1 << num;

            PlayerPrefs.SetInt("BlockPossession", blockPossession);
        }
        else
        {
            if ((bgPossession >> num) % 2 == 1) return;
            bgPossession += 1 << num;

            PlayerPrefs.SetInt("BgPossession", bgPossession);
        }
        PlayerPrefs.Save();
    }

    public void ResetPurchase()
    {
        curCat = 0;
        PlayerPrefs.SetInt("CurCat", 0);
        PlayerAnimation.Instance.SetCatNum(0);

        curBlock = 0;
        PlayerPrefs.SetInt("CurBlock", 0);
        SpriteManager.Instance.SetBlockSkin(0);

        curBG = 0;
        PlayerPrefs.SetInt("CurBG", 0);
        Background.Instance.SetSprite();

        catPossession = 1;
        PlayerPrefs.SetInt("CatPossession", 1);
        blockPossession = 1;
        PlayerPrefs.SetInt("BlockPossession", 1);
        bgPossession = 1;
        PlayerPrefs.SetInt("BgPossession", 1);
    }

    public string GetName(int type, int num)
    {
        if (type == 0)
        {
            return catNames[num];
        }
        else if (type == 1)
        {
            return blockNames[num];
        }
        else
        {
            return BGNames[num];
        }
    }

    public Sprite GetSprite(int type, int num)
    {
        if (type == 0)
        {
            return PlayerAnimation.Instance.GetSprite(num);
        }
        else if (type == 1)
        {
            return SpriteManager.Instance.GetSprite(num, 0);
        }
        else
        {
            return Background.Instance.GetSprite(num);
        }
    }

    public void ShowItemPanel(int type, int num, ShopItem shop)
    {
        // itemPanel.gameObject.SetActive(true);
        PopupManager.Instance.PopupActive(itemPanel.gameObject, true);
        PopupManager.Instance.ShowBackground();
        itemPanel.SetData(type, num, shop);
    }

    public void ShowNoMoneyPanel()
    {
        PopupManager.Instance.PopupActive(noMoneyPanel, true);
    }


}
