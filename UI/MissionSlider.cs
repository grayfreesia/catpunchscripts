﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionSlider : MonoBehaviour
{
    public Image fill;
    private float value;
    public Text missionObjectText;

    public void SetValue(float v)
    {
        value = v;
        fill.fillAmount = v;
    }

    public void SetMissionObject(int num)
    {
        missionObjectText.text = num.ToString();
    }

    public float GetValue()
    {
        return value;
    }
}
