﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopBase : MonoBehaviour
{
    RectTransform rectTransform;
    float prev = 1f;
    public int type = 0;
    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        SpawnItems();
    }

    void Update()
    {
        Refresh();
    }

    void Refresh()
    {
        float ratio = Screen.safeArea.height / Screen.safeArea.width;
        if(ratio != prev)
        {
            ApplyShopPanelSize(ratio);
            prev = ratio;
        }
    }

    private void ApplyShopPanelSize(float ratio)
    {
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, 600f * ratio);
    }

    private void SpawnItems()
    {
        if(type == 3) return;
        GameObject firstChild = GetComponentInChildren<ShopItem>().gameObject;
        Transform itemContainer = firstChild.transform.parent;
        for(int i = 1; i < ShopDataManager.Instance.GetItemLength(type); i++)
        {
            Instantiate(firstChild, itemContainer);
        }
    }
}
