﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAnimation : MonoBehaviour
{
    public Sprite[] sprites;
    private int num = 0;
    private Image image;
    private Coroutine ani;

    void Awake()
    {
        image = GetComponent<Image>();
    }

    void OnEnable()
    {
        ani = StartCoroutine(AniStart());
    }

    void OnDisable()
    {
        StopCoroutine(ani);
    }

    IEnumerator AniStart()
    {
        while (true)
        {
            num++;
            if (num >= sprites.Length) num = 0;
            image.sprite = sprites[num];
            yield return new WaitForSeconds(.5f);
        }
    }

}
