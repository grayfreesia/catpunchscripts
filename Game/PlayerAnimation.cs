﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Sprite[] sprites;
    private const int spritePerCat = 6;
    private int catNum = 0;
    private SpriteRenderer spriteRenderer;
    private Coroutine coroutine;
    private Coroutine shadow;
    private enum State { Idle, Left, Right, Fail };
    private State state = State.Idle;
    private bool isStateChange = false;
    private bool isFail = false;

    public static PlayerAnimation Instance;

    void Awake()
    {
        if(Instance == null) Instance = this;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        SetCatNum(ShopDataManager.Instance.curCat);
        // coroutine = StartCoroutine(Idle());
        ShadowBoxing();
    }

    void Update()
    {
        Controll();
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
    }

    public void OnStart()
    {
        StopShadowBoxing();
        isFail = false;
        state = State.Idle;
        if(coroutine != null) StopCoroutine(coroutine);
        coroutine = StartCoroutine(Idle());
    }

    private void Controll()
    {
        if (isStateChange && !isFail)
        {
            isStateChange = false;
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }
            switch (state)
            {
                case State.Idle:
                    coroutine = StartCoroutine(Idle());
                    break;
                case State.Left:
                    coroutine = StartCoroutine(Left());
                    break;
                case State.Right:
                    coroutine = StartCoroutine(Right());
                    break;
                case State.Fail:
                    coroutine = StartCoroutine(Fail());
                    break;
                default:
                    break;
            }
        }
    }

    public void ChangeState(int num)
    {
        if (state != (State)num)
        {
            state = (State)num;
            isStateChange = true;
        }

    }

    IEnumerator Idle()
    {
        bool flag = false;
        while (true)
        {
            if (flag)
            {
                spriteRenderer.sprite = sprites[catNum * spritePerCat + 0];
            }
            else
            {
                spriteRenderer.sprite = sprites[catNum * spritePerCat + 1];
            }
            flag = !flag;
            yield return new WaitForSeconds(.4f);
        }
    }

    IEnumerator Left()
    {
        spriteRenderer.sprite = sprites[catNum * spritePerCat + 2];
        yield return new WaitForSeconds(.2f);
        ChangeState(0);
    }

    IEnumerator Right()
    {
        spriteRenderer.sprite = sprites[catNum * spritePerCat + 3];
        yield return new WaitForSeconds(.2f);
        ChangeState(0);
    }

    IEnumerator Fail()
    {
        isFail = true;
        transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        spriteRenderer.sprite = sprites[catNum * spritePerCat + 4];
        yield return new WaitForSeconds(.5f);
        spriteRenderer.sprite = sprites[catNum * spritePerCat + 5];
    }

    public Sprite GetSprite(int num)
    {
        if(sprites.Length <= num * spritePerCat) return null;
        else return sprites[num * spritePerCat];
    }

    public void SetCatNum(int num)
    {
        catNum = num;
        // OnStart();
        ShadowBoxing();
    }
    
    public void ShadowBoxing()
    {
        if(shadow != null) StopCoroutine(shadow);
        shadow = StartCoroutine(ShadowBoxingCR());
    }

    public void StopShadowBoxing()
    {
        if(shadow != null) StopCoroutine(shadow);
    }

    IEnumerator ShadowBoxingCR()
    {
        // Debug.Log("Shada");
        isFail = false;
        spriteRenderer.sprite = sprites[catNum * spritePerCat + 0];
        yield return new WaitForSeconds(.3f);

        while(true)
        {
            Vector3 scale = Player.Instance.transform.localScale;
            
            
            ChangeState(1);
            yield return new WaitForSeconds(.3f);
            ChangeState(2);
            yield return new WaitForSeconds(.3f);
            ChangeState(1);
            yield return new WaitForSeconds(.3f);
            ChangeState(2);
            yield return new WaitForSeconds(.3f);

            ChangeState(0);
            yield return new WaitForSeconds(.5f);
            Player.Instance.transform.Translate(.1f,0,0);
            ChangeState(1);
            yield return new WaitForSeconds(.5f);
            Player.Instance.transform.Translate(.1f,0,0);
            ChangeState(2);
            yield return new WaitForSeconds(.5f);
            ChangeState(1);
            yield return new WaitForSeconds(.5f);
            Player.Instance.transform.Translate(.1f,0,0);
            ChangeState(2);
            yield return new WaitForSeconds(1f);
            Player.Instance.transform.localScale = new Vector3(-scale.x, scale.y, scale.z);

            
            Player.Instance.transform.Translate(-.1f,0,0);
            ChangeState(1);
            yield return new WaitForSeconds(.3f);
            Player.Instance.transform.Translate(-.1f,0,0);
            ChangeState(2);
            yield return new WaitForSeconds(.5f);
            Player.Instance.transform.localScale = scale;
            ChangeState(1);
            yield return new WaitForSeconds(.3f);
            Player.Instance.transform.Translate(-.1f,0,0);
            ChangeState(2);
            yield return new WaitForSeconds(.3f);
            Player.Instance.transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
            yield return new WaitForSeconds(1f);
            ChangeState(0);
            yield return new WaitForSeconds(.5f);
        }
    }
}
