﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopItem : MonoBehaviour, IPointerClickHandler
{
    private enum State { Selected, CanUse, Purchase, Lock };
    private State state = State.Purchase;
    public GameObject coinIcon;
    // public GameObject lockIcon;
    public Text buttonText;
    public Sprite[] buttonSprites;
    public Image buttonImage;
    public Sprite[] itemBGSprites;
    public Image itemBGImage;
    public Image itemImage;
    public Text itemName;
    public Text lockText;

    private int type = 0;
    private int num = 0;
    private int price = 3000;
    private bool isPurchased = false;
    private int isLock = -1;
    private bool isMissionLock = false;

    private string choosen;
    private string choice;
    private string quest;
    private string unlock;

    void Start()
    {
        // InitItem();
    }

    void OnEnable()
    {
        // if (DebugButtonShow.Instance != null) 
        SetString();
        InitItem();
    }

    void SetString()
    {
        choosen = LanguageManager.GetString(0, 7);
        choice = LanguageManager.GetString(0, 8);
        quest = LanguageManager.GetString(0, 9);
        unlock = LanguageManager.GetString(0, 10);
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        OnClickButton();
    }

    private void InitItem()
    {
        type = transform.parent.parent.parent.GetComponent<ShopBase>().type;
        num = transform.GetSiblingIndex();
        
        LanguageManager.Instance.SetFont(itemName ,lockText, buttonText);
        price = ShopDataManager.Instance.GetPrice(type, num);
        isPurchased = ShopDataManager.Instance.IsPurchased(type, num);
        itemName.text = ShopDataManager.Instance.GetName(type, num);
        itemImage.sprite = ShopDataManager.Instance.GetSprite(type, num);
        isLock = ShopDataManager.Instance.IsLock(type, num);
        if (ShopDataManager.Instance.IsSelected(type, num))
        {
            SetState(State.Selected);
        }
        else if (isPurchased)
        {
            SetState(State.CanUse);
        }
        else if (isLock < 0)
        {
            isMissionLock = true;
            SetState(State.Lock);
        }
        else if (isLock > 0)
        {
            SetState(State.Lock);
        }
        else
        {
            SetState(State.Purchase);
        }

    }

    public void OnClickButton()
    {
        // SetState((State)(((int)state+1 )%3));
        if (state == State.Selected || state == State.Lock) return;
        else if (state == State.CanUse)
        {
            ShopDataManager.Instance.GetCurItem(type).SetState(State.CanUse);

            SetState(State.Selected);
        }
        else if (state == State.Purchase)
        {
            // if (Gold.Instance.SubGold(price))
            // {
            ShopDataManager.Instance.ShowItemPanel(type, num, this);
            // }
        }

    }

    public void OnBuy()
    {
        ShopDataManager.Instance.GetCurItem(type).SetState(State.CanUse);
        SetState(State.Selected);
        ShopDataManager.Instance.SetPossessionTrue(type, num);
    }

    private void SetState(State newState)
    {
        state = newState;
        switch (state)
        {
            case State.Selected:
                buttonText.text = choosen;
                // buttonText.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                itemImage.color = Color.white;
                coinIcon.SetActive(false);
                // lockIcon.SetActive(false);
                lockText.text = "";
                ShopDataManager.Instance.SetCurrentItem(type, num, this);
                break;
            case State.CanUse:
                buttonText.text = choice;
                // buttonText.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                itemImage.color = Color.white;
                coinIcon.SetActive(false);
                // lockIcon.SetActive(false);
                lockText.text = "";
                break;
            case State.Purchase:
                buttonText.text = "      " + string.Format("{0:#,###0}", price);
                // buttonText.GetComponent<RectTransform>().anchoredPosition = new Vector2(30, 0);
                itemImage.color = Color.gray;
                coinIcon.SetActive(true);
                // lockIcon.SetActive(false);
                lockText.text = "";
                break;
            case State.Lock:
                buttonText.text = "";
                // buttonText.GetComponent<RectTransform>().anchoredPosition = new Vector2(30, 0);
                itemImage.color = Color.gray;
                coinIcon.SetActive(false);
                // lockIcon.SetActive(true);
                if (isMissionLock)
                {
                    lockText.text = string.Format(quest, -isLock, "\n");
                }
                else
                {
                    lockText.text = string.Format(unlock, isLock, "\n");
                }
                break;
            default:
                break;
        }

        buttonImage.sprite = buttonSprites[(int)state];
        itemBGImage.sprite = itemBGSprites[type < 2 ? (int)state : (int)state == 0 ? 4 : 5];//배경 구매시엔 테두리 바뀌어야함
    }
}
