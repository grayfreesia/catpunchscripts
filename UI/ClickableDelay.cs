﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickableDelay : MonoBehaviour
{
    public float delay = 1.2f;
    private Image[] images;
    void Awake()
    {
        images = GetComponentsInChildren<Image>();
    }

    void OnEnable()
    {
        SetButtonActive(false);
        Invoke("SetButtonActiveTrue", delay);
    }

    private void SetButtonActive(bool on)
    {
        foreach (Image image in images)
        {
            image.raycastTarget = on;
        }
    }

    private void SetButtonActiveTrue()
    {
        SetButtonActive(true);
    }



}
