﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugButtonShow : MonoBehaviour
{
    public DebugButton debug;
    public bool rewardDebug = false;
    public static DebugButtonShow Instance;

    private bool zeroFlag = false;
    private int click = 0;

    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Update()
    {
        if(click > 2)
        {
            DebugPanelShow();
            click = 0;
        }
        if(click > 0 && !zeroFlag)
        {
            zeroFlag = true;
            Invoke("SetZero", 1f);
        }

        
    }

    public void DebugButtonOnOff(bool on)
    {
        debug.gameObject.SetActive(on);
        // debug.OnClickDebug();
    }

    public void DebugPanelShow()
    {
        debug.OnClickDebug();
    }

    public void FastDebugClick()
    {
        click++;
    }

    private void SetZero()
    {
        click = 0;
        zeroFlag = false;
    }

}
