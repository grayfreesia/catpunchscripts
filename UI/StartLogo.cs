﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLogo : MonoBehaviour
{
    public void MoveUp()
    {
        iTween.MoveBy(gameObject, iTween.Hash("amount", Vector3.up * Screen.height, "time", 2f, "oncomplete", "MoveEnd"));
    }

    private void MoveEnd()
    {
        transform.parent.gameObject.SetActive(false);
    }
}
