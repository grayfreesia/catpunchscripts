﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FreeGift : MonoBehaviour
{
    public Text header;
    public Text timeText;
    public Text goldAmountText;
    public Text moreGoldText;
    public Text confirmText;

    private string timeTextString = "다음 선물까지 남은시간";
    private int goldAmount = 0;
    private int moreGoldTime = 2;

    void Update()
    {
        SetText();
    }

    void OnEnable()
    {
        SetGoldAmount();
        SetMoreGoldTime();
        TimeManager.Instance.SetWaitAmount(UnityEngine.Random.Range(1, 7));
        TimeManager.Instance.SetLastTime();
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        SetString();
    }

    void OnDisable()
    {
        BackButtonManager.Instance.OnClose();
    }

    public void SetString()
    {
        LanguageManager.Instance.SetFont(header, timeText, confirmText);
        header.text = LanguageManager.GetString(0, 1);
        timeTextString = LanguageManager.GetString(0, 2);
        confirmText.text = LanguageManager.GetString(0, 3);
    }

    public void OnClickGet()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
        Gold.Instance.GiftEffect();
        //
        Gold.Instance.AddGold(goldAmount, 1.1f);
        // gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
    }

    public void OnClickMoreGold()
    {
        //reward ads
#if UNITY_EDITOR
        goldAmount *= moreGoldTime;
        OnClickGet();
#else
        AdsBackground.Instance.ShowBackground(true);
        AdSystem.ShowAd(()=>{
            AdsBackground.Instance.ShowBackground(false);
            goldAmount *= moreGoldTime;
            OnClickGet();
        });
#endif

    }



    public void SetText()
    {
        string s = "";
        if (TimeManager.Instance.remain >= TimeSpan.Zero)
        {
            s = TimeManager.Instance.remain.ToString();
        }
        timeText.text = timeTextString + "\n" + s;
    }

    public void SetGoldAmount()
    {
        // Debug.Log(TimeManager.Instance.wait);
        if (TimeManager.Instance.wait < 0 || TimeManager.Instance.wait > 6) return;
        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            goldAmount = 50 + (TimeManager.Instance.wait - 1) * 20;
        }
        else
        {
            goldAmount = 100 + (TimeManager.Instance.wait - 1) * 30;
        }
        goldAmountText.text = "X" + goldAmount;
    }

    public void SetMoreGoldTime()
    {
        int r = UnityEngine.Random.Range(0, 20);
        if (r < 1)
        {
            moreGoldTime = 4;
        }
        else if (r < 6)
        {
            moreGoldTime = 3;
        }
        else
        {
            moreGoldTime = 2;
        }
        moreGoldText.text = "X" + moreGoldTime;
    }




}
