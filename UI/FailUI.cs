﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FailUI : MonoBehaviour
{
    public Text best;
    public Text score;
    public Text gold;

    public GameObject shop;
    // public GameObject setting;
    public GameObject freeGift;
    public GameObject rewardGift;
    public GameObject continuePanel;

    public GameObject settingButton;
    public GameObject settingButton_ios;
    public GameObject leaderboardButton;

    public FailUIAnimation[] animations;

    void OnEnable()
    {
        int bestScore = PlayerPrefs.GetInt("BestScore", 0);
        int currentScore = Point.Instance.point;
        PlayerPrefs.SetInt("TotalScore",PlayerPrefs.GetInt("TotalScore", 0) + Point.Instance.point);

        if(currentScore > bestScore)
        {
            bestScore = currentScore;
            PlayerPrefs.SetInt("BestScore", bestScore);
            PlayerPrefs.Save();
        }

        score.text = "SCORE : " + string.Format("{0:#,###0}", currentScore);
        best.text = "TOP " + string.Format("{0:#,###0}", bestScore);
        gold.text = "Gold : " + string.Format("{0:#,###0}", gold);

        #if UNITY_ANDROID
        settingButton.SetActive(true);
        settingButton_ios.SetActive(false);
        leaderboardButton.SetActive(true);
        #else
        settingButton.SetActive(false);
        settingButton_ios.SetActive(true);
        leaderboardButton.SetActive(false);
        #endif

    }

    public void OnClickRetry()
    {
        GameManager.Instance.StartGame();
        #if UNITY_EDITOR
        #else
        if(Random.Range(0f,100f) < GameManager.Instance.interstitalProb)
        {
            // InterstitialAdTest.Instance.ShowInterstitial();
            InterstitialAdmob.Instance.ShowAd();
        } 
            
        #endif
    }

    public void OnClickShop()
    {
        shop.SetActive(true);
    }

    public void OnClickContinue()
    {
        //video ads
        // continuePanel.SetActive(true)
        PopupManager.Instance.PopupActive(continuePanel, true);
    }

    public void OnClickFreeGift()
    {
        // freeGift.SetActive(true);
        PopupManager.Instance.PopupActive(freeGift, true);
    }

    public void OnClickRewardGift()
    {
        #if UNITY_EDITOR
        // rewardGift.SetActive(true);
        PopupManager.Instance.PopupActive(rewardGift, true);
        #else
        AdsBackground.Instance.ShowBackground(true);
        AdSystem.ShowAd(() =>
        {
            AdsBackground.Instance.ShowBackground(false);
            // rewardGift.SetActive(true);
            PopupManager.Instance.PopupActive(rewardGift, true);
        });
        #endif
    }

}
