﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageManager : MonoBehaviour
{
    static List<Dictionary<string, object>> lang;
    static List<Dictionary<string, object>> cat;
    static List<Dictionary<string, object>> block;
    static List<Dictionary<string, object>> bg;
    static List<Dictionary<string, object>> pack;
    static List<Dictionary<string, object>> price;
    public int num = 0;
    private string[] langCodes = {"ko", "en", "ja", "zh-Hans", "zh-Hant"};
    public Font[] fonts;
    public static string language = "ko";

    public delegate void OnChangeEvent();
    public static event OnChangeEvent OnChange;   

    public static LanguageManager Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
        lang = CSVReader.Read("CSVs/lang");
        cat = CSVReader.Read("CSVs/cat");
        block = CSVReader.Read("CSVs/block");
        bg = CSVReader.Read("CSVs/bg");
        pack = CSVReader.Read("CSVs/pack");
        price = CSVReader.Read("CSVs/price");
        num = PlayerPrefs.GetInt("Language", -1);
        if(num == -1) FirstInitLanguage();
        language = langCodes[num];
    }

    public void OnChangeLanguage()
    {
        num = (num+1)%langCodes.Length;
        language = langCodes[num];
        OnChange();
    }

    public void OnChangeLanguage(int value)
    {
        if(value >= langCodes.Length) return;
        num = value;
        language = langCodes[num];
        OnChange();
    }

    private void FirstInitLanguage()
    {
        switch(Application.systemLanguage)
        {
            case SystemLanguage.Korean:
                num = 0;
                PlayerPrefs.SetInt("Language", 0);
                break;
            case SystemLanguage.English:
                num = 1;
                PlayerPrefs.SetInt("Language", 1);
                break;
            case SystemLanguage.Japanese:
                num = 2;
                PlayerPrefs.SetInt("Language", 2);
                break;
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
                num = 3;
                PlayerPrefs.SetInt("Language", 3);
                break;
            case SystemLanguage.ChineseTraditional:
                num = 4;
                PlayerPrefs.SetInt("Language", 4);
                break;
            default:
                num = 1;
                PlayerPrefs.SetInt("Language", 1);
                break;

        }
    }

    public static string GetString(int type, int num)
    {
        if(type == 0)
        {
            return lang[num][language].ToString();
        }
        else if(type == 1)
        {
            return cat[num][language].ToString();
        }
        else if(type == 2)
        {
            return block[num][language].ToString();
        } 
        else if(type == 3)
        {
            return bg[num][language].ToString();
        } 
        else if(type == 4)
        {
            return pack[num][language].ToString();
        } 
        else if(type == 5)
        {
            return price[num][language].ToString();
        } 
        else return "error";
    }

    public string[] GetLangNames()
    {
        string[] names = new string[langCodes.Length];
        for(int i = 0; i < langCodes.Length; i++)
        {
            names[i] = lang[0][langCodes[i]].ToString();
        }
        return names;
    }

    public void SaveLangSetting()
    {
        PlayerPrefs.SetInt("Language", num);
        PlayerPrefs.Save();
    }

    public void SetFont(params Text[] texts)
    {
        foreach(Text text in texts)
        {
            text.font = fonts[num];
        }
    }

    
}
