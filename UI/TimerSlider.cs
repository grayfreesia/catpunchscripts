﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSlider : MonoBehaviour
{
    // private float value = 1f;
    public Image bg;
    public Image fill;
    public Image ice;
    public Sprite[] sprites;

    private bool isIce = false;

    void Update()
    {
        if(isIce && Timer.Instance.isRunning) ice.fillAmount = BlockItemSkill.Instance.GetIceTimeRatio();
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
    }

    public void OnStart()
    {
        IceImage(false);
    }

    public void SetValue(float v)
    {
        if (v < 0f || v > 1f) return;
        // value = v;
        fill.fillAmount = v;
    }

    public void IceImage(bool on)
    {
        fill.sprite = sprites[on ? 1 : 0];
        bg.sprite = sprites[on ? 3 : 2];
        if(on)
        {
            //Sound
            
        }
        else
        {
            ice.fillAmount = 0f;
        }
        isIce = on;
    }

    
}
