﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gold : MonoBehaviour
{
    private int gold;
    public int gameGold;
    private Text text;
    public GameObject goldPrefab;
    public static Gold Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        text = GetComponentInChildren<Text>();
        gold = PlayerPrefs.GetInt("Gold", 0);
        // text.text = gold.ToString();
        text.text = string.Format("{0:#,###0}", gold);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            BlockCoinEffect();
        }
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
        GameManager.OnEnd += OnEnd;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
        GameManager.OnEnd -= OnEnd;
    }

    public void OnStart()
    {
        gameGold = 0;
    }

    public void OnEnd()
    {
        SaveGold();
    }

    public int GetGold()
    {
        return gold;
    }

    public void SetGold(int value)
    {
        gold = value;
        text.text = gold.ToString();
    }

    public void AddGold(int value)
    {
        gold += value;
        gameGold += value;
        SaveGold();
        // text.text = string.Format("{0:#,###0}", gold);
    }

    public void AddGold(int value, float wait)
    {
        gold += value;
        gameGold += value;
        // if(value < 10)
        // {
        //     Invoke("GoldTextDelay",wait);
        // }
        // else
        {
            GoldNumTween(gold-value, gold, wait);
        }
        SaveGold();
        SoundManager.Instance.Play(3);
    }

    public bool SubGold(int value)
    {
        if (value > gold) return false;
        gold -= value;
        // if(value < 100)
        // {
        // }
        // else
        {
            GoldNumTween(gold+value, gold, 0f);
        }
        SaveGold();
        return true;
    }

    public void SaveGold()
    {
        PlayerPrefs.SetInt("Gold", gold);
        PlayerPrefs.Save();
    }

    public void ResetGold()
    {
        gold = 0;
        text.text = gold.ToString();
        SaveGold();
    }

    public void GiftEffect()
    {
        StartCoroutine(GiftEffectCR());
    }

    IEnumerator GiftEffectCR()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject g = Instantiate(goldPrefab, transform);
            g.GetComponent<GiftEffectCoin>().SetStartPos(new Vector2(Random.Range(50f, 350f), -Random.Range(350f, 800f)), true);
            yield return new WaitForSeconds(.1f);
        }
    }

    public void BlockCoinEffect()//Block block)    
    {
        GameObject g = Instantiate(goldPrefab, transform);
        // Vector2 blockPos = block.transform.position;
        // Vector2 blockPos = Camera.main.WorldToViewportPoint(block.transform.position);
        // Debug.Log(block.transform.position + "ps" + blockPos);
        float y = -.57f * 800f * Screen.safeArea.height/Screen.safeArea.width;
        g.GetComponent<GiftEffectCoin>().SetStartPos(new Vector2(250,y), false);
    }

    public void GoldNumTween(int from, int target, float wait)
    {
        float time = (target - from) == 1 ? 0f:1f;
        iTween.ValueTo(gameObject, iTween.Hash(
            "delay", wait,
            "from", from,
            "to", target,
            "time", time,
            "onupdate", "tweenOnUpdateCallBack",
            "easetype", iTween.EaseType.linear
            )
        );
    }

    // public void GoldTextDelay()
    // {
    //     text.text = string.Format("{0:#,###0}", gold);
    // }

    void tweenOnUpdateCallBack(int newValue)
    {
        text.text = string.Format("{0:#,###0}", newValue);
    }
}
