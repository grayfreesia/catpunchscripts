﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour
{
    // private Action Temp;
    public GameObject popupBackground;
    public GameObject popupBackgroundPrefab;
    private Stack<GameObject> pbstack;
    private static GameObject toOff;

    public GameObject adsFailPanel;
    public static PopupManager Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        pbstack = new Stack<GameObject>();
    }

    public void ShowBackground(params bool[] condition)
    {
        // popupBackground.SetActive(true);
        GameObject back = Instantiate(popupBackgroundPrefab, popupBackground.transform);
        pbstack.Push(back);
        pbstack.Peek().GetComponent<Button>().onClick.AddListener(BackButtonManager.Instance.Back);
        if(condition.Length > 0 && condition[0] == false)
        {
            back.GetComponent<Button>().interactable = false;
        }

    }

    // public void ShowBackground(Action action)
    // {
    //     // popupBackground.SetActive(true);
    //     pbstack.Push(Instantiate(popupBackgroundPrefab, popupBackground.transform));
    //     // Temp = action;
    // }

    public void OffBackground()
    {
        // popupBackground.SetActive(false);
        if (pbstack.Count > 0)
        {
            Destroy(pbstack.Pop());
        }

    }

    public void OnClickBackground()
    {
        BackButtonManager.Instance.OnClickPopupBackground();
        // if(Temp != null)
        // {
        //     Debug.Log("temp");
        //     BackButtonManager.Instance.SetAction(Temp);
        //     Temp = null;
        // }

        // else 
        // BackButtonManager.Instance.OnClose();
        OffBackground();
    }

    public void PopupActive(GameObject panel, bool on)
    {
        if (on)
        {
            panel.SetActive(true);
            panel.transform.localScale = Vector3.one;
            iTween.ScaleFrom(panel, iTween.Hash(
                "time", .2f,
                "scale", Vector3.zero,
                "easetype", iTween.EaseType.easeOutBack
            ));
        }
        else
        {
            toOff = panel;
            Invoke("Off", .2f);
            iTween.ScaleTo(panel, iTween.Hash(
                "time", .2f,
                "scale", Vector3.zero,
                "easetype", iTween.EaseType.easeInBack
            // "oncomplete", "Off"
            ));

        }
    }

    public void Off()
    {
        toOff.SetActive(false);
    }

    public void ShowAdsFail()
    {
        PopupActive(adsFailPanel, true);
    }

}
