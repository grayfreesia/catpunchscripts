﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveLoadPanel : MonoBehaviour
{
    public Text message;
    public Text confirm;
    public GameObject button;
    public GameObject loading;
    public GameObject bg;

    private string[] logs = { "로그인실패", "저장실패", "로딩실패", "저장성공" };

    public static SaveLoadPanel Instance;

    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void Update()
    {
        loading.transform.Rotate(Vector3.forward * Time.deltaTime * -200f);
    }

    void OnEnable()
    {
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        SetString();
        // SetState(SettingSaveLoad.Instance.num);
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(confirm);
        for (int i = 0; i < logs.Length; i++)
        {
            logs[i] = LanguageManager.GetString(0, 25 + i);
        }
        confirm.text = LanguageManager.GetString(0, 15);
    }

    public void OnClickConfirm()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
        // gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
    }

    public void SetState(int num)
    {
        if (num < 0)
        {
            message.text = "";
            button.SetActive(false);
            loading.SetActive(true);
            bg.SetActive(false);
        }
        else
        {
            message.text = logs[num];
            button.SetActive(true);
            loading.SetActive(false);
            bg.SetActive(true);

        }
        // switch(num)
        // {

        //     case 0 :
        //     text.text = "saving..";
        //     button.SetActive(false);
        //     break;

        //     case 1 :
        //     text.text = "loading..";
        //     button.SetActive(false);
        //     break;

        //     case 2 :
        //     text.text = "Login Fail";
        //     button.SetActive(true);
        //     break;

        //     case 3 :
        //     text.text = "Save Fail";
        //     button.SetActive(true);
        //     break;

        //     case 4 :
        //     text.text = "Load Fail";
        //     button.SetActive(true);
        //     break;

        //     case 5 :
        //     text.text = "Save Success";
        //     button.SetActive(true);
        //     break;

        //     case 6 :
        //     text.text = "Authenticate Fail";
        //     button.SetActive(true);
        //     break;
        // }
    }
}
