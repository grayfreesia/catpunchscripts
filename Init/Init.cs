﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Init : MonoBehaviour
{
    // public Image fill;
    void Start()
    {
        // if (PlayerPrefs.GetInt("IsFirstLoad", 1) == 1)
        // {
        //     PlayerPrefs.SetInt("IsFirstLoad", 0);
        //     SceneManager.LoadScene("Main");
        // }

        StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Main");


        while(!operation.isDone)
        {
            Debug.Log(operation.progress);
            // fill.fillAmount = operation.progress;
            yield return null;
        }
        // float amount = 0f;
        // while(amount < 1f)
        // {
        //     amount += 0.02f;
        //     fill.fillAmount = amount;
        //     yield return null;
        // }
    }

}
