﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardGift : MonoBehaviour
{
    public Text titleText;
    public Text getText;
    public Text goldAmountText;

    private int goldAmount = 0;
    private int firstGift = 0;
    void Awake()
    {
        firstGift = PlayerPrefs.GetInt("FirstGift", 1);
        if (firstGift == 1)
        {
            PlayerPrefs.SetInt("FirstGift", 0);
            PlayerPrefs.Save();
        }
    }

    void OnEnable()
    {
        SetGoldAmount();
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        SetString();

    }

    void OnDisable()
    {
        BackButtonManager.Instance.OnClose();
    }

    public void SetString()
    {
        LanguageManager.Instance.SetFont(titleText, getText);
        titleText.text = LanguageManager.GetString(0, 6);
        getText.text = LanguageManager.GetString(0, 3);
    }

    public void OnClickGet()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
        Gold.Instance.GiftEffect();
        //
        Gold.Instance.AddGold(goldAmount, 1.1f);//천천히 오르는 기능 추가해야함
        // gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
    }


    public void SetGoldAmount()
    {
        // Debug.Log(TimeManager.Instance.wait);
        int r = UnityEngine.Random.Range(0, 20);
        if(firstGift == 1)
        {
            if(r < 8)
            {
                goldAmount = 400;
            }
            else if(r < 16)
            {
                goldAmount = 500;
            }
            else
            {
                goldAmount = 600;
            }
        }
        else
        {
            if(r < 6)
            {
                goldAmount = 200;
            }
            else if(r < 12)
            {
                goldAmount = 300;
            }
            else if(r < 18)
            {
                goldAmount = 400;
            }
            else if(r < 19)
            {
                goldAmount = 500;
            }
            else
            {
                goldAmount = 600;
            }
        }
        goldAmountText.text = "X" + goldAmount;
    }

}
