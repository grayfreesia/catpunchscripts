﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public Vector2Int pos;
    public GameObject coin;
    // public Sprite[] sprites;
    public GameObject particle;
    public GameObject punchEffect;
    public GameObject goldEffect;
    public bool isTurnPos = false;
    public bool isMarked = false;
    // private int type = 0;
    public enum BlockType { Basic, Gold, GoldBonus, Ice, AutoPunch};//, Drink, Super, Marker };
    public BlockType type = BlockType.Basic;
    private float[] probability = {.05f, .005f, .005f, .005f};//, .005f, .005f, .005f};
    private SpriteRenderer spriteRenderer;
    // private int blockSkin = 0;
    public static float probMax = 1f;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        SetType();
    }

    private void SetType()
    {
        float r = Random.Range(0f, probMax);
        // r *= 100f;
        // float p = .05f + Point.Instance.point / (Point.Instance.point + 1000);

        // if (isTurnPos)
        // {
        //     type = BlockType.Basic;
        if(isMarked) SetMarked(true);
        // }
        
        float p = 0f;
        for(int i = 0; i < probability.Length; i++)
        {
            p += probability[i];
            if(r < p)
            {
                type = (BlockType)(i+1);
                break;
            }
        }
        
        SetBlockShape();
    }

    public void SetMarked(bool on)
    {
        if (isTurnPos)
        {
            if (on)
            {
                // renderer.sprite = SpriteManager.Instance.markedBlock[blockSkin];
                spriteRenderer.color = Color.red;
            }
            else
            {
                // renderer.sprite = SpriteManager.Instance.basicBlock[blockSkin];
                spriteRenderer.color = Color.white;
            }
        }
    }


    public void BlockDestroy()
    {
        ItemActivate();
        Instantiate(particle, transform.position, Quaternion.identity);
        Vector2 effectPos = (Player.Instance.transform.position + transform.position)/2f;
        float range = 0.2f;
        effectPos += new Vector2(Random.Range(-range, range), Random.Range(-range, range));

        Instantiate(punchEffect, effectPos, Quaternion.identity);
        Destroy(gameObject);
    }

    public void ItemActivate()
    {
        switch (type)
        {
            case BlockType.Gold:
                Gold.Instance.AddGold(1, .6f);
                Gold.Instance.BlockCoinEffect();
                break;
            case BlockType.GoldBonus:
                BlockItemSkill.Instance.GoldBonus();
                break;
            case BlockType.AutoPunch:
                BlockItemSkill.Instance.AutoPunch();
                break;
            case BlockType.Ice:
                BlockItemSkill.Instance.IceTime();
                break;
            // case BlockType.Drink:
            //     BlockItemSkill.Instance.EnergyDrink();
            //     break;
            // case BlockType.Super:
            //     BlockItemSkill.Instance.SuperMode();
            //     break;
            // case BlockType.Marker:
            //     BlockMarker.Instance.MarkBlock();
            //     break;
            
            default:
                break;
        }
        // Debug.Log(type);
    }

    public void SetBlockShape()
    {
        //gold 도 기본 이미지 씀
        if(type == BlockType.Gold)
        {
            spriteRenderer.sprite = SpriteManager.Instance.GetSprite(SpriteManager.Instance.blockSkin, (int)BlockType.Basic);
            coin.SetActive(true);
        }
        else
        {
            spriteRenderer.sprite = SpriteManager.Instance.GetSprite(SpriteManager.Instance.blockSkin, (int)type);
            coin.SetActive(false);
        }
        
    }

    public void ChangeToGold()
    {
        if(type == BlockType.Basic || type == BlockType.Gold)
        {
            type = BlockType.Gold;
            spriteRenderer.sprite = SpriteManager.Instance.GetSprite(SpriteManager.Instance.blockSkin, (int)type);
            coin.SetActive(false);
            GameObject effect = Instantiate(goldEffect, transform.position, Quaternion.identity);
            effect.transform.localScale = transform.localScale;
        }
    }
    
}
