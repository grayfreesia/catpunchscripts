﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background : MonoBehaviour
{
    public GameObject back;
    public GameObject ground;
    public GameObject backTile;
    public GameObject groundTile;
    private GameObject[] backs = new GameObject[5];
    private GameObject[] grounds = new GameObject[5];
    public Image[] buttonsImages;
    public Sprite[] backSprites;
    public Sprite[] groundSprites;
    public Sprite[] buttonsSprites;

    private const float backAmount = .1f;
    private const float groundAmount = .7f;
    private float backSize = 0f;
    private float groundSize = 0f;
    private Vector3 targetBackPos;
    private Vector3 originBackPos;
    private Vector3 targetGroundPos;
    private Vector3 originGroundPos;
    public static Background Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        originBackPos = back.transform.position;
        targetBackPos = originBackPos;
        originGroundPos = ground.transform.position;
        targetGroundPos = originGroundPos;
        SetChildrenPos();
    }

    void Start()
    {
        SetSprite();
    }

    private void SetChildrenPos()
    {
        backSize = backTile.GetComponent<SpriteRenderer>().size.x;
        groundSize = groundTile.GetComponent<SpriteRenderer>().size.x;
        backs[0] = backTile;
        grounds[0] = groundTile;
        for(int i = 1; i < 5; i++)
        {
            backs[i] = Instantiate(backTile, backTile.transform.parent);
            grounds[i] = Instantiate(groundTile, groundTile.transform.parent);
        }
        for(int i = 0; i < 5; i++)
        {
            backs[i].transform.localPosition = new Vector2((i - 2) * backSize, 0);
            grounds[i].transform.localPosition = new Vector2((i - 2) * groundSize, 0);
        }

    }

    public void Move(Vector2 dir)
    {
        MoveBack(dir);
        MoveGround(dir);
    }

    private void MoveBack(Vector2 dir)
    {
        targetBackPos += (Vector3)dir * backAmount;
        // iTween.Stop(back);
        iTween.MoveTo(back, iTween.Hash("time", 0.2f, "position", targetBackPos));
        // back.transform.Translate(dir);
        ResetBackPos();
    }

    private void MoveGround(Vector2 dir)
    {
        targetGroundPos += (Vector3)dir * groundAmount;
        iTween.MoveTo(ground, iTween.Hash("time", 0.2f, "position", targetGroundPos));
        ResetGroundPos();
    }

    private void ResetBackPos()
    {
        float diff = back.transform.localScale.x * backSize;
        if (back.transform.position.x > diff)
        {
            back.transform.Translate(-diff, 0, 0);
            targetBackPos += new Vector3(-diff, 0, 0);
            iTween.Stop(back);

        }
        else if (back.transform.position.x < -diff)
        {
            back.transform.Translate(diff, 0, 0);
            targetBackPos += new Vector3(diff, 0, 0);
            iTween.Stop(back);

        }
    }

    private void ResetGroundPos()
    {
        float diff = ground.transform.localScale.x * groundSize;
        if (ground.transform.position.x > diff)
        {
            ground.transform.Translate(-diff, 0, 0);
            targetGroundPos += new Vector3(-diff, 0, 0);
            iTween.Stop(ground);

        }
        else if (ground.transform.position.x < -diff)
        {
            ground.transform.Translate(diff, 0, 0);
            targetGroundPos += new Vector3(diff, 0, 0);
            iTween.Stop(ground);

        }
    }

    public void SetSprite()
    {
        Sprite backSprite = backSprites[ShopDataManager.Instance.curBG];
        Sprite groundSprite = groundSprites[ShopDataManager.Instance.curBG];
        foreach (GameObject b in backs)
        {
            b.GetComponent<SpriteRenderer>().sprite = backSprite;
        }
        foreach (GameObject g in grounds)
        {
            g.GetComponent<SpriteRenderer>().sprite = groundSprite;
        }

        buttonsImages[0].sprite = buttonsSprites[ShopDataManager.Instance.curBG * 2];
        buttonsImages[1].sprite = buttonsSprites[ShopDataManager.Instance.curBG * 2 + 1];

    }

    public Sprite GetSprite(int num)
    {
        if (backSprites.Length <= num) return null;
        return backSprites[num];
    }


}
