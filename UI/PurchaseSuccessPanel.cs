﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseSuccessPanel : MonoBehaviour
{
    public Text message;
    public Text confirm;

    void OnEnable()
    {
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        SetString();
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(message, confirm);
        message.text = LanguageManager.GetString(0, 14);
        confirm.text = LanguageManager.GetString(0, 15);
    }

    public void OnClickConfirm()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
        // gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
    }
}
