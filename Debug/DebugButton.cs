﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugButton : MonoBehaviour
{
    public bool toggle = false;
    public GameObject debugPanel;

    public Text blockDiffText;
    private int blockDiff = 0;

    public Text timeDiffText;
    private int timeDiff = 0;

    public Text assistModeText;
    private bool isAssistMode = false;
    
    public Text itemModeText;
    private bool isItemMode = false;

    public Text safeAreaText;
    private bool isSafaArea = false;

    public Text freeGiftText;
    private bool isFreeGiftEasy = false;

    public GameObject freeGift;
    public GameObject rewardGift;

    public GameObject totalScorePanel;
    private bool isTotalOn = false;

    public Text freePurchase;
    private bool isFreePurchase = false;

    public void Start()
    {
        gameObject.SetActive(false);
    }

    public void OnClickDebug()
    {
        toggle = !toggle;
        debugPanel.SetActive(toggle);
    }

    public void SetBlockDifficulty()
    {
        //1번 버튼
        float diff = 1f;
        string s = "easy";
        blockDiff = (blockDiff+1)%3;
        switch(blockDiff)
        {
            case 0:
                diff = .1f;
                s = "Easy";
                break;
            case 1:
                diff = .5f;
                s = "Normal";
                break;
            case 2:
                diff = 1f;
                s = "Hard";
                break;
            default:
                break;

        }
        blockDiffText.text = "블럭난이도:" + s;
        Map.Instance.SetDifficulty(diff);
        GameManager.Instance.StartGame();
    }

    public void SetTimeDifficulty()
    {
        //2번 버튼
        string s = "easy";
        timeDiff = (timeDiff+1)%3;
        switch(timeDiff)
        {
            case 0:
                s = "Easy";
                break;
            case 1:
                s = "Normal";
                break;
            case 2:
                s = "Hard";
                break;
            default:
                break;

        }
        timeDiffText.text = "시간난이도:" + s;
        Timer.Instance.SetTimeDifficulty(timeDiff);
        GameManager.Instance.StartGame();
    }

    public void SetAssistMode()
    {
        isAssistMode = !isAssistMode;
        string s = isAssistMode?"On":"Off";
        assistModeText.text = "assist :" + s;
        BlockMarker.Instance.SetAssistMode(isAssistMode);
        GameManager.Instance.StartGame();
        
    }

    public void ResetGold()
    {
        PlayerPrefs.SetInt("TotalScore", 0);
        Gold.Instance.ResetGold();
    }

    public void AddGold()
    {
        Gold.Instance.AddGold(10000, 0f);
        Gold.Instance.SaveGold();
    }

    public void SetItemMode()
    {
        isItemMode = !isItemMode;
        string s = isItemMode?"On":"Off";
        itemModeText.text = "ItemMode :" + s;
        if(isItemMode)
        {
            Block.probMax = .15f;
        }
        else
        {
            Block.probMax = 1f;
        }
        GameManager.Instance.StartGame();
    }

    // public void SetSafeArea()
    // {
    //     isSafaArea = !isSafaArea;
    //     string s =isSafaArea?"On":"Off";
    //     safeAreaText.text = "SafaArea :" + s;
    //     PlayerPrefs.SetInt("IsDebugSafeArea", isSafaArea?1:0);
    //     Debug.Log("set safe area"+(isSafaArea?1:0));
    // }

    public void SetFreeGiftTime()
    {
        isFreeGiftEasy = !isFreeGiftEasy;
        string s =isFreeGiftEasy?"10초":"6시간";
        freeGiftText.text = "무료선물시간 :" + s;
        TimeManager.Instance.SetWaitAmount(isFreeGiftEasy?0:6);
        TimeManager.Instance.SetServerTime();
    }

    public void ShowFreeGift()
    {
        // freeGift.SetActive(true);
        PopupManager.Instance.PopupActive(freeGift, true);
    }

    public void ShowRewardGift()
    {
        // rewardGift.SetActive(true);
        PopupManager.Instance.PopupActive(rewardGift, true);
    }

    public void ResetPurchase()
    {
        ShopDataManager.Instance.ResetPurchase();
        PurchaseManager.Instance.ResetPackPurchase();
    }

    public void ResetBestScore()
    {
        PlayerPrefs.SetInt("BestScore", 0);
    }

    public void ReloadScene()//reset all
    {
        ResetBestScore();
        ResetGold();
        ResetPurchase();
        ResetMission();
        PlayerPrefs.SetInt("Language", -1);
        PlayerPrefs.SetInt("IsFirstLoad", 1);
        SceneManager.LoadScene("Main");
    }

    public void AlwayseRewardBanner()
    {
        // RewardPanel.Instance.SetDebug();
        DebugButtonShow.Instance.rewardDebug = true;
    }

    public void ShowTotalPanel()
    {
        isTotalOn = !isTotalOn;
        totalScorePanel.SetActive(isTotalOn);
    }

    public void ShowAd(int num)
    {
        AdsBackground.Instance.ShowBackground(true);
        AdSystem.DebugShowAd(()=>{
            AdsBackground.Instance.ShowBackground(false);
        },
        num);
    }

    public void DebugTutorial()
    {
        Tutorial.Instance.DebugTutorial();
    }
    
    public void DebugPointAdd()
    {
        if(Point.Instance != null) Point.Instance.DebugAddPoint();
    }

    public void SetFreePurchase()
    {
        isFreePurchase = !isFreePurchase;
        string s = isFreePurchase?"On":"Off";
        freePurchase.text = "무료구매 :" + s;
        PurchaseManager.Instance.isDebug = isFreePurchase;
    }

    public void ResetMission()
    {
        if(Mission.Instance != null) Mission.Instance.ResetMission();
    }

    public void MissionClear()
    {
        if(Mission.Instance != null) Mission.Instance.MissionClear();
    }

    public void Logout()
    {
        #if UNITY_ANDROID
        if(GPGSManager.Instance != null) GPGSManager.Instance.SignOut();
        #endif
    }

    public void CloudDelete()
    {
        #if UNITY_ANDROID
        if(GPGSManager.Instance != null) GPGSManager.Instance.DebugDelete();
        #endif
    }

    public void ShowSaveSelect()
    {
        #if UNITY_ANDROID
        if(GPGSManager.Instance != null) GPGSManager.Instance.ShowSelectUI();
        #endif
    }


    


}
