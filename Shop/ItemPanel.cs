﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPanel : MonoBehaviour
{
    public Text text;
    public Text name;
    public Text priceText;
    public Text cancelText;
    public Text buyText;
    public Image itemImage1;
    public Image itemImage2;
    private ShopItem shopItem;
    private int price;

    void OnEnable()
    {
        BackButtonManager.Instance.SetAction(OnBack);
        SetString();
        // PopupManager.Instance.SetTempAction(ShopPanel.Instance.OnClickExit);
    }

    void OnDisable()
    {
        // BackButtonManager.Instance.SetAction(ShopPanel.Instance.OnClickExit);
        // PopupManager.Instance.OffBackground();
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(text, cancelText, buyText);
        text.text = LanguageManager.GetString(0, 11);
        cancelText.text = LanguageManager.GetString(0, 12);
        buyText.text = LanguageManager.GetString(0, 13);
    }

    public void OnClickClose()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
// gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
        PopupManager.Instance.OffBackground();
    }

    public void SetData(int type, int num, ShopItem shop)
    {
        price = ShopDataManager.Instance.GetPrice(type, num);
        priceText.text = string.Format("{0:#,###0}", price);
        name.text = ShopDataManager.Instance.GetName(type, num);
        // text.text = "'" + ShopDataManager.Instance.GetName(type, num) + "'을(를)\n구매할거냥?";
        if (type == 2)
        {
            itemImage1.gameObject.SetActive(false);
            itemImage2.transform.parent.gameObject.SetActive(true);
            itemImage2.sprite = ShopDataManager.Instance.GetSprite(type, num);

        }
        else
        {
            itemImage2.transform.parent.gameObject.SetActive(false);
            itemImage1.gameObject.SetActive(true);
            itemImage1.sprite = ShopDataManager.Instance.GetSprite(type, num);
            if(type == 0)
            {
                itemImage1.transform.localScale = Vector2.one;
            }
            else
            {
                itemImage1.transform.localScale = Vector2.one * .5f;
            }
        }
        shopItem = shop;
    }

    public void OnClickBuy()
    {
        if(shopItem != null) 
        {
            if (Gold.Instance.SubGold(price))
            {
                shopItem.OnBuy();
                OnClickClose();
            }
            else
            {
                ShopDataManager.Instance.ShowNoMoneyPanel();
            }
        }
    }
}
