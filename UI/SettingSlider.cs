﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SettingSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler//, IPointerEnterHandler, IPointerExitHandler
{
    private float value = 1f;
    public GameObject knob;
    public Image fill;
    private RectTransform knobRT;
    private bool isDown = false;
    private float width = 0f;
    void Awake()
    {
        width = GetComponent<RectTransform>().sizeDelta.x * .9f;
        knobRT = knob.GetComponent<RectTransform>();
    }
    void Update()
    {
        if (isDown)
        {
            knob.transform.position = new Vector2(Input.mousePosition.x, knob.transform.position.y);
            if(knobRT.anchoredPosition.x > width/2f)
            {
                knobRT.anchoredPosition = new Vector2(width/2f, 0);
            }
            else if(knobRT.anchoredPosition.x < -width/2f)
            {
                knobRT.anchoredPosition = new Vector2(-width/2f, 0);
            }
            value = (knobRT.anchoredPosition.x + width/2f)/ width;
            fill.fillAmount = value;

        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
    }

    public float GetValue()
    {
        return value;
    }

    public void SetValue(float v)
    {
        value = v;
        knobRT.anchoredPosition = new Vector2((value-.5f)*width,0);
        fill.fillAmount = value;
    }

}
