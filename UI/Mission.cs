﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission : MonoBehaviour
{
    private int missionNum = 0;
    public int missionObjectNum = 0;
    public MissionSlider slider;
    public MissionSuccessPanel missionSuccessPanel;
    public static Mission Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
        GameManager.OnEnd += OnEnd;

    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
        GameManager.OnEnd -= OnEnd;
    }

    void OnStart()
    {
        missionNum = PlayerPrefs.GetInt("MissionNum", 1);
        SetMissionObject();
        SetSliderValue(0);
    }

    void OnEnd()
    {
        if (slider.GetValue() >= 1f)
        {
            MissionClear();
        }
    }

    private int GetMissionObject()
    {
        if (missionNum < 11)
        {
            return missionNum * 100;
        }
        else if (missionNum < 19)
        {
            return 1000 + (missionNum - 10) * 500;
        }
        else
        {
            return 5000 + (missionNum - 18) * 1000;
        }
    }

    private void SetMissionObject()
    {
        missionObjectNum = GetMissionObject();//test/10  
        slider.SetMissionObject(missionObjectNum);
    }

    public void SetSliderValue(int point)
    {
        if (point > missionObjectNum)
        {
            slider.SetValue(1f);
        }
        else
        {
            slider.SetValue((float)point / missionObjectNum);
        }
    }

    private void SetMissionSuccessPanelData()
    {
        int type = -1;
        int num = 0;
        if (missionNum == 2)
        {
            type = 1;
            num = 13;
        }
        else if (missionNum == 4)
        {
            type = 0;
            num = 10;
        }
        else if (missionNum == 6)
        {
            type = 1;
            num = 14;
        }
        else if (missionNum == 8)
        {
            type = 0;
            num = 11;
        }
        else if (missionNum == 10)
        {
            type = 2;
            num = 6;
        }
        else if (missionNum == 12)//2000
        {
            type = 1;
            num = 15;
        }
        else if (missionNum == 14)//3000
        {
            type = 0;
            num = 12;
        }
        else if (missionNum == 16)//4000
        {
            type = 1;
            num = 16;
        }
        else if (missionNum == 18)//5000
        {
            type = 0;
            num = 13;
        }
        else
        {
            num = missionObjectNum * 10;
        }
        missionSuccessPanel.SetData(type, num);
    }

    public void ResetMission()
    {
        missionNum = 1;
        PlayerPrefs.SetInt("MissionNum", 1);
        SetMissionObject();
    }

    public void MissionClear()
    {
        Debug.Log("Mission clear!");
        PopupManager.Instance.PopupActive(missionSuccessPanel.gameObject, true);
        SetMissionSuccessPanelData();
        PlayerPrefs.SetInt("MissionNum", ++missionNum);
        SetMissionObject();
    }
}
