﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackButtonManager : MonoBehaviour
{
    // private Action Back;
    public Text quitText;
    public Text noText;
    public Text yesText;
    private Stack<Action> actions;
    public GameObject quitPanel;
    private const float backButtonDelay = .21f;
    private float time = 0f;
    private bool isAble = false;
    public static BackButtonManager Instance;

    void Awake()
    {
        if (Instance == null) Instance = this;
        actions = new Stack<Action>();
        // SetAction();
    }

    void Update()
    {
        TimeCal();
        if (Input.GetKeyDown(KeyCode.Escape) && isAble)
        {
            // OnClose();
            Back();
            // PopupManager.Instance.OffBackground();
        }

    }

    public void SetAction(Action backButton)
    {
        // Back = backButton;
        actions.Push(backButton);
    }

    public void OnClose()
    {
        // Back();
    }

    public void QuitPanelOn()
    {
        // quitPanel.SetActive(true);
        PopupManager.Instance.PopupActive(quitPanel, true);
        SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        SetStringQuit();


        if (Timer.Instance != null && Timer.Instance.isRunning && !Player.Instance.isFail) Timer.Instance.isRunning = false;

        // isTimeStop = true;
        // Invoke("TimeStop", 0.21f);
    }

    // private void TimeStop()
    // {
    //     if (Timer.Instance != null && Timer.Instance.isRunning && isTimeStop) Time.timeScale = 0f;
    // }

    public void OnClcikQuit()
    {
        Application.Quit();
    }

    public void OnClickQuitClose()
    {
        OnBack();
        BackByClick();
    }

    public void OnBack()
    {
        // quitPanel.SetActive(false);
        PopupManager.Instance.PopupActive(quitPanel, false);
        // SetAction();
        if (Timer.Instance != null && !Timer.Instance.isRunning && !Player.Instance.isFail) Timer.Instance.isRunning = true;

        // if (Timer.Instance != null && Timer.Instance.isRunning) Time.timeScale = 1f;
    }

    public void OnClickPopupBackground()
    {
        Back();
    }

    public void Back()
    {
        if (actions.Count > 0)
        {
            Action action = actions.Pop();
            action();
            PopupManager.Instance.OffBackground();
        }
        else
        {
            QuitPanelOn();
        }
        isAble = false;
    }

    public void BackByClick()
    {
        if (actions.Count > 0)
        {
            actions.Pop();
            PopupManager.Instance.OffBackground();
        }
    }

    private void TimeCal()
    {
        if(isAble) return;
        if (time > backButtonDelay)
        {
            isAble = true;
            time = 0f;
        }
        else
        {
            time += Time.deltaTime;
        }
    }

    private void SetStringQuit()
    {
        LanguageManager.Instance.SetFont(quitText, noText, yesText);
        quitText.text = LanguageManager.GetString(0, 16);
        noText.text = LanguageManager.GetString(0, 17);
        yesText.text = LanguageManager.GetString(0, 18);
    }


}
