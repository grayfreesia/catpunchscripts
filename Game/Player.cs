﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private const float playerSize = .4f;
    public Vector2Int pos = Vector2Int.zero;
    public bool dir = true;//right
    // public Sprite[] sprites;
    // public Sprite[] catSprites;
    // public int catNum = 0;
    private int spriteNum = 0;
    public bool punchable = true;
    private Vector3 originPos;

    public bool isFail = true;

    public static Player Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        transform.localScale = Vector3.one * Map.size * playerSize;
        originPos = transform.position;
    }

    void Update()
    {
        //test
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            PunchButton.Instance.OnClickPunch();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            TurnButton.Instance.OnClickTurn();
        }

    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
        GameManager.OnEnd += OnEnd;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
        GameManager.OnEnd -= OnEnd;
    }

    public void OnStart()
    {
        transform.localScale = Vector3.one * Map.size * playerSize;
        // spriteRenderer.sprite = sprites[0];
        transform.position = originPos;
        dir = true;
        pos = Vector2Int.zero;
        spriteNum = 0;
        punchable = true;
        isFail = false;
    }

    public void OnEnd()
    {
        punchable = false;
        isFail = true;
    }

    public void OnContinue()
    {
        dir = !dir;
        transform.localScale = new Vector3(dir ? 1 : -1, 1, 1) * Map.size * playerSize;
        punchable = true;
    }

    public void Punch()
    {
        ChangeSprite();

        if (Map.Instance.GetNextBlockPos().x < pos.x && !dir ||
        Map.Instance.GetNextBlockPos().x > pos.x && dir)
        {
            PunchSuccess();
        }
        else
        {
            PunchFail();
        }

    }

    public void Turn()
    {
        dir = !dir;
        transform.localScale = new Vector3(dir ? 1 : -1, 1, 1) * Map.size * playerSize;
        Punch();
    }

    private void PunchSuccess()
    {
        if(!Timer.Instance.isRunning) Timer.Instance.isRunning = true;
        SoundManager.Instance.Play(0);
        Timer.Instance.AddTime();
        Point.Instance.AddPoint();
        Map.Instance.DestroyBlock();
    }

    private void PunchFail()
    {
        // Debug.Log("Fail");
        // Map.Instance.Move(Vector2Int.down);
        GameManager.Instance.FailGame();
    }

    private void ChangeSprite()
    {
        if (spriteNum == 0)
        {
            spriteNum = 1;
        }
        else
        {
            spriteNum = 0;
        }
        // spriteRenderer.sprite = sprites[spriteNum];
        PlayerAnimation.Instance.ChangeState(spriteNum + 1);

    }

    

}
