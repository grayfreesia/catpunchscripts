﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPanel : MonoBehaviour
{
    public Image[] tabImages;
    public GameObject[] panels;
    public Text headerText;//test
    public GameObject bg;
    public Sprite[] headerSprites;
    public Image headerImage;

    private int curPanel = 0;

    public static ShopPanel Instance;

    void Awake()
    {
        if(Instance == null) Instance = this;
        
    }

    // void Start()
    // {
    //     OnClickTab(curPanel);
    //     gameObject.SetActive(false);
    // }

    void OnEnable()
    {
        bg.SetActive(true);
        BackButtonManager.Instance.SetAction(OnBack);
        ChangeHeader(curPanel);
        // OnClickTab(curPanel);
        LanguageManager.Instance.SetFont(headerText);
    }

    void OnDisable()
    {
        bg.SetActive(false);
        BackButtonManager.Instance.OnClose();
    }

    public void OnClickTab(int num)
    {
        for(int i = 0; i < panels.Length; i++)
        {
            if(i == num)
            {
                panels[i].SetActive(true);
                tabImages[i].color = Color.white;
            } 
            else 
            {
                panels[i].SetActive(false);
                tabImages[i].color = Color.gray;
            }
        }
        ChangeHeader(num);
        curPanel = num;
    }

    public void OnClickExit()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
        gameObject.SetActive(false);
    }

    private void ChangeHeader(int num)
    {
        switch(num)
        {
            case 0:
                headerText.text = LanguageManager.GetString(1, 0);
                break;
            case 1:
                headerText.text = LanguageManager.GetString(2, 0);
                break;
            case 2:
                headerText.text = LanguageManager.GetString(3, 0);
                break;
            case 3:
                headerText.text = LanguageManager.GetString(4, 0);
                break;
            default:
                break;
        }
        headerImage.sprite = headerSprites[num];
    }

}
