﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject ingameUI;
    public GameObject failUI;
    public GameObject notOffUI;

    public delegate void OnStartEvent();
    public static event OnStartEvent OnStart;    
    public delegate void OnEndEvent();
    public static event OnEndEvent OnEnd;

    public bool isContinued = false;
    public float interstitalProb = 10f;
    public static GameManager Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void StartGame()
    {
        ingameUI.SetActive(true);
        failUI.SetActive(false);
        notOffUI.SetActive(true);

        OnStart();

        isContinued = false;
        // InterstitialAdTest.Instance.LoadInterstitial();

    }

    public void FailGame()
    {
        failUI.SetActive(true);
        ingameUI.SetActive(false);

        PlayerAnimation.Instance.ChangeState(3);
        SoundManager.Instance.Play(1);

        OnEnd();
        
        
    }

    public void Continue()
    {
        failUI.SetActive(false);
        ingameUI.SetActive(true);

        // Timer.Instance.OnStart();
        //reduce 초기화인데 조금 고민
        // Map.Instance.ContinueUp();
        // BlockItemSkill.Instance.OnStart();
        // Player.Instance.punchable = true;
        Timer.Instance.OnContinue();
        Player.Instance.OnContinue();
        isContinued = true;
        PlayerAnimation.Instance.OnStart();
    }

    
}
