﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinuePanel : MonoBehaviour
{
    public Text titleText;

    void OnEnable()
    {
        BackButtonManager.Instance.SetAction(OnBack);
        PopupManager.Instance.ShowBackground();
        SetString();
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(titleText);
        titleText.text = LanguageManager.GetString(0, 4);
    }

    public void OnClickCancel()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnClickByAds()
    {

#if UNITY_EDITOR
        GameManager.Instance.Continue();
        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore", 0) - Point.Instance.point);
        OnClickCancel();

#else
        AdsBackground.Instance.ShowBackground(true);
        AdSystem.ShowAd(()=>
        {
            AdsBackground.Instance.ShowBackground(false);
            GameManager.Instance.Continue();
            PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore", 0) - Point.Instance.point);

            OnClickCancel();
        });
#endif
    }

    public void OnClickByCoin()
    {
        if (Gold.Instance.SubGold(200))
        {
            GameManager.Instance.Continue();
            PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore", 0) - Point.Instance.point);
            OnClickCancel();
        }
        else
        {
            ShopDataManager.Instance.ShowNoMoneyPanel();
        }
    }

    public void OnBack()
    {
        // gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
        // PopupManager.Instance.OffBackground();
    }
}
