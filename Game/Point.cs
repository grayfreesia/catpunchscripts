﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Point : MonoBehaviour
{
    public int point = 0;
    private Text text;
    public static Point Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
        text = GetComponent<Text>();
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
    }

    public void OnStart()
    {
        point = 0;
        // text.text = point.ToString();
        text.text = string.Format("{0:#,###0}",point);
    }

    public void AddPoint()
    {
        point++;
        // text.text = point.ToString();
        text.text = string.Format("{0:#,###0}",point);
        Map.Instance.AddDifficulty(.0005f);
        // if(point % 100 == 0) Fever();
        if(Mission.Instance != null) Mission.Instance.SetSliderValue(point);
    }   

    public void DebugAddPoint()
    {
        point += 1000;
        // text.text = point.ToString();
        text.text = string.Format("{0:#,###0}",point);
    }

    // public void Fever()
    // {
    //     foreach(Block block in Map.Instance.blocks)
    //     {
    //         block.ChangeToGold();
    //     }
    // }


}
