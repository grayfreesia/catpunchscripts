﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPosHelper : MonoBehaviour
{
    private const float c = 200f;
    private const float s = .5f;
    private RectTransform rectTransform;
    private Vector2 originPos;
    private float prev = 1f;
    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        originPos = rectTransform.anchoredPosition;
        Refresh();
    }

    void Update()
    {
        Refresh();
    }

    void Refresh()
    {
        float ratio = ((float)Screen.width/(float)Screen.height);
        if(ratio != prev)
        {
            ApplyPos(ratio);
            prev = ratio;
        }
    }

    void ApplyPos(float ratio)
    {
        Vector2 pos = new Vector2(originPos.x, originPos.y - c * ratio);
        rectTransform.anchoredPosition = pos;
        if(ratio == 0) Debug.Log("ratio 0");
        else transform.localScale = Vector2.one * s * (1f/ratio);
    }
}
