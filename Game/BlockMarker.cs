﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMarker : MonoBehaviour
{
    private bool isAssistMode = false;
    private bool isMarkingMode = false;
    private float markingTime = 10f;
    public static BlockMarker Instance;
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Update()
    {
        MarkTime();
    }

    void OnEnable()
    {
        GameManager.OnStart += OnStart;
    }

    void OnDisable()
    {
        GameManager.OnStart -= OnStart;
    }

    public void OnStart()
    {
        MarkBlockOff();
    }

    public void BlockTurnMarkCheck(Block newBlock, int num)
    {
        if (num == 0 || num == 2)
        {
            if (Map.Instance.dir)
            {
                if (isAssistMode || isMarkingMode) newBlock.isMarked = true;//setmarked대신, 블럭에서 초기화하기때문
                newBlock.isTurnPos = true;
            }
            Map.Instance.dir = false;//left
        }
        else if (num == 1 || num == 4)
        {
            if (!Map.Instance.dir)
            {
                if (isAssistMode || isMarkingMode) newBlock.isMarked = true;
                newBlock.isTurnPos = true;
            }
            Map.Instance.dir = true;//right
        }
    }

    public void SetAssistMode(bool b)//Debug용 영구적
    {
        isAssistMode = b;
    }

    public void MarkBlock()
    {
        // float markBlockTime = 10f;
        markingTime = 10f;
        if(isMarkingMode) return;
       
        isMarkingMode = true;
        for(int i = 0; i < Map.Instance.blocks.Count; i++)
        {
            Map.Instance.blocks[i].SetMarked(true);
        }
    }

    public void MarkBlockOff()
    {
        isMarkingMode = false;
        if(isAssistMode) return;
        for(int i = 0; i < Map.Instance.blocks.Count; i++)
        {
            Map.Instance.blocks[i].SetMarked(false);
        }
    }

    public void MarkTime()
    {
        if(isMarkingMode)
        {
            markingTime -= Time.deltaTime;
            if(markingTime < 0f)
            {
                MarkBlockOff();
            }
        }
    }
}
