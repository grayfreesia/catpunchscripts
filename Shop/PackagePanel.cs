﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PackagePanel : MonoBehaviour
{
    private PurchaseItem purchaseItem;
    private int price;
    public Sprite[] packCatSprites;
    public Sprite[] packBlockSprites;
    public Sprite[] packBGSprites;
    public Sprite[] packCoinSprites;

    private string[] packCatName = {"어흥냥이", "달콤냥이", "수험생냥이"};
    private string[] packBlockName = {"고기블럭", "하트블럭", "책블럭"};
    private string[] packBGName = {"정글배경", "하트배경", "도시배경"};

    private int[] discounts = {63, 65, 70};
    private int[] coinAmount = {8000, 8000, 32000};
    private string[] originCost = {"₩16250", "₩17000", "₩33250"};
    private string[] cost = {"₩5900", "₩5900", "₩9900"};

    public Image catImage;
    public Image blockImage;
    public Image bgImage;
    public Image coinImage;

    public Text title;
    public Text catText;
    public Text blockText;
    public Text bgText;
    public Text coinText;
    public Text discountText;
    public Text costText;
    public Text returnCoinText;
    public Text oneTimeText;

    private string returnCoinString = "이미 보유한 아이템은 코인으로 환급됩니다.";
    // private string oneTime = "1회만 구매가능";
    private string coinString = "코인";


    void OnEnable()
    {
        BackButtonManager.Instance.SetAction(OnBack);
        Invoke("SetData", .01f);
        SetString();
    }

    private void SetString()
    {
        LanguageManager.Instance.SetFont(title, oneTimeText, catText, blockText, bgText, costText, returnCoinText);
        for(int i = 0; i < packCatName.Length; i++)
        {
            packCatName[i] = LanguageManager.GetString(4, i*4 + 5);
            packBlockName[i] = LanguageManager.GetString(4, i*4 + 6);
            packBGName[i] = LanguageManager.GetString(4, i*4 + 7);
        }
        title.text = LanguageManager.GetString(0, 11);
        returnCoinString = LanguageManager.GetString(4, 3);
        oneTimeText.text = LanguageManager.GetString(4, 2);
        coinString = LanguageManager.GetString(4, 1);
        for(int i = 0; i < originCost.Length; i++)
        {
            originCost[i] = LanguageManager.GetString(5, i*2);
            cost[i] = LanguageManager.GetString(5, i*2+1);
        }
    }


    public void OnClickClose()
    {
        OnBack();
        BackButtonManager.Instance.BackByClick();
    }

    public void OnBack()
    {
// gameObject.SetActive(false);
        PopupManager.Instance.PopupActive(gameObject, false);
        PopupManager.Instance.OffBackground();
    }

    public void OnClickBuy()
    {
        if(purchaseItem != null) 
        {
            #if UNITY_EDITOR
            PurchaseManager.Instance.OnPurchase(purchaseItem.num);
            OnClickClose();
            PurchaseManager.Instance.SetPackPurchased(purchaseItem.num);
            purchaseItem.OffItem();
            PurchaseManager.Instance.ShowPurchaseSuccessPanel();
            #else
            PurchaseManager.Instance.OnClickPurchase(purchaseItem.num, ()=>{
                OnClickClose();
                PurchaseManager.Instance.SetPackPurchased(purchaseItem.num);
                purchaseItem.OffItem();
                PurchaseManager.Instance.ShowPurchaseSuccessPanel();
            });
            #endif
            

        }
    }

    private void SetData()
    {
        int n = purchaseItem.num - PurchaseManager.packStartNum;
        // title.text = "'" + packCatName[n] +"'을(를)\n구매할거냥?";
        catText.text = packCatName[n] + " X1";
        blockText.text = packBlockName[n] + " X1";
        bgText.text = packBGName[n] + " X1";
        coinText.text =  coinString +" X" + string.Format("{0:#,###0}", coinAmount[n]);
        discountText.text = "'" + discounts[n] + "%\nOFF!";
        costText.text = originCost[n] + "\n" +cost[n];

        int returnCoin = GetReturnCoin(n);
        if(returnCoin > 0)
        {
            returnCoinText.text = returnCoinString +"\n (+" + returnCoin + ")";
        }
        else
        {
            returnCoinText.text = "";
        }

        catImage.sprite = packCatSprites[n];
        blockImage.sprite = packBlockSprites[n];
        bgImage.sprite = packBGSprites[n];
        coinImage.sprite = packCoinSprites[n];
        
    }

    public void SetPackage(PurchaseItem item)
    {
        purchaseItem = item;
    }

    public int GetReturnCoin(int packNum)
    {
        int coin = 0;
        if(packNum == 0)//어흥냥이팩
        {
            coin += GetRetunrPrice(0, 8);
            coin += GetRetunrPrice(1, 10);
            coin += GetRetunrPrice(2, 3);
        }
        else if(packNum == 1)//달콤냥이팩
        {
            coin += GetRetunrPrice(0, 7);
            coin += GetRetunrPrice(1, 11);
            coin += GetRetunrPrice(2, 4);
            
        }
        else if(packNum == 2)//수험생냥이팩
        {
            coin += GetRetunrPrice(0, 9);
            coin += GetRetunrPrice(1, 12);
            coin += GetRetunrPrice(2, 5);
            
        }
        
        return coin;
    }

    public int GetRetunrPrice(int type, int num)
    {
        if (ShopDataManager.Instance.IsPurchased(type, num))
        {
            return ShopDataManager.Instance.GetPrice(type, num);
        }
        else
        {
            return 0;
        }
    }

}
